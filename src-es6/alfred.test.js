import moment from 'moment';

import Alfred from '../';

describe('Alfred', () => {
  describe('isObject', () => {
    it('returns false if input is not provided', () => {
      expect(Alfred.isObject()).toBe(false);
    });
    it('returns false if input is undefined', () => {
      expect(Alfred.isObject(undefined)).toBe(false);
    });
    it('returns false if input is null', () => {
      expect(Alfred.isObject(null)).toBe(false);
    });
    it('returns false if input is number', () => {
      expect(Alfred.isObject(1)).toBe(false);
    });
    it('returns false if input is string', () => {
      expect(Alfred.isObject('object')).toBe(false);
    });
    it('returns false if input is array', () => {
      expect(Alfred.isObject(['bla', { foo: 'bar' }])).toBe(false);
    });
    it('returns true if input is an object', () => {
      expect(Alfred.isObject({ foo: 'bar' })).toBe(true);
    });
    it('returns true if input is an object defined using Object constructor', () => {
      // eslint-disable-next-line
      expect(Alfred.isObject(new Object(undefined))).toBe(true);
    });
  });
  describe('validateShape', () => {
    const shape = {
      id: {
        optional: false,
      },
      name: {
        optional: false,
      },
      image: {
        optional: true,
      },
    };
    it('returns false if object is not an object', () => {
      const object = [];
      expect(Alfred.validateShape(object, shape)).toBe(false);
    });
    it('returns false if object does not have compulsary keys', () => {
      const object = {
        id: 'Foo Bar',
        image: 'https://for.bar/image.png',
      };
      expect(Alfred.validateShape(object, shape)).toBe(false);
    });
    it('returns false if object has extra keys', () => {
      const object = {
        id: '12345',
        name: 'Foo Bar',
        profile_picture: 'https://for.bar/image.png',
      };
      expect(Alfred.validateShape(object, shape)).toBe(false);
    });
    it('returns true if optional keys are missing', () => {
      const object = {
        id: '12345',
        name: 'Foo Bar',
      };
      expect(Alfred.validateShape(object, shape)).toBe(true);
    });
    it('returns true if it has extra keys but mode is set to easy', () => {
      const object = {
        id: '12345',
        name: 'Foo Bar',
        profile_picture: 'https://for.bar/image.png',
      };
      expect(Alfred.validateShape(object, shape, 'easy')).toBe(true);
    });
  });
  describe('padString', () => {
    it('pad string where string is lower than expected', () => {
      const expectedPaddedString = '0001';

      expect(Alfred.padString('1', 4, '0', 'start')).toEqual(expectedPaddedString);
    });
    it('does not pad string where string is more than expected', () => {
      const expectedPaddedString = '1234';

      expect(Alfred.padString('1234', 4, '0', 'start')).toEqual(expectedPaddedString);
    });
    it('Pads at the end of string', () => {
      const expectedPaddedString = '1000';

      expect(Alfred.padString('1', 4, '0', 'end')).toEqual(expectedPaddedString);
    });
  });
  describe('extractBaseUrl', () => {
    it('throws error if url not provided', () => {
      expect(() => Alfred.extractBaseUrl()).toThrow('Cannot extract base url from undefined');
    });
    it('throws error if url sent is not a string', () => {
      expect(() => Alfred.extractBaseUrl({
        url: 'https://juerix.com',
      })).toThrow('Can only extract base url from string');
    });
    it('returns base url', () => {
      const url = 'https://juerix.com/earrings/nice-silver-earrings';
      const expectedBaseUrl = 'https://juerix.com';
      const extractedBaseUrl = Alfred.extractBaseUrl(url);

      expect(extractedBaseUrl).toEqual(expectedBaseUrl);
    });
    it('returns base url and ignores port', () => {
      const url = 'https://juerix.com:8080/earrings/nice-silver-earrings';
      const expectedBaseUrl = 'https://juerix.com';
      const extractedBaseUrl = Alfred.extractBaseUrl(url);

      expect(extractedBaseUrl).toEqual(expectedBaseUrl);
    });
    it('works even if scheme not provided in url', () => {
      const url = 'juerix.com:8080/earrings/nice-silver-earrings';
      const expectedBaseUrl = 'juerix.com';
      const extractedBaseUrl = Alfred.extractBaseUrl(url);

      expect(extractedBaseUrl).toEqual(expectedBaseUrl);
    });
  });
  describe('extractPath', () => {
    it('throws error if url not provided', () => {
      expect(() => Alfred.extractPath()).toThrow('Cannot extract path from undefined');
    });
    it('throws error if url sent is not a string', () => {
      expect(() => Alfred.extractPath({
        url: 'https://juerix.com',
      })).toThrow('Can only extract path from string');
    });
    it('returns path', () => {
      const url = 'https://juerix.com/earrings/nice-silver-earrings';
      const expectedBaseUrl = '/earrings/nice-silver-earrings';
      const extractedBaseUrl = Alfred.extractPath(url);

      expect(extractedBaseUrl).toEqual(expectedBaseUrl);
    });
    it('returns path with port', () => {
      const url = 'https://juerix.com:8080/earrings/nice-silver-earrings';
      const expectedBaseUrl = ':8080/earrings/nice-silver-earrings';
      const extractedBaseUrl = Alfred.extractPath(url);

      expect(extractedBaseUrl).toEqual(expectedBaseUrl);
    });
    it('extracts the first char from path if removeFirstChar is passed as true', () => {
      const url = 'https://juerix.com/earrings/nice-silver-earrings';
      const expectedBaseUrl = 'earrings/nice-silver-earrings';
      const extractedBaseUrl = Alfred.extractPath(url, true);

      expect(extractedBaseUrl).toEqual(expectedBaseUrl);
    });
  });
  describe('chop', () => {
    it('throws error if string to chop is undefined', () => {
      expect(() => Alfred.chop()).toThrow('Cannot chop undefined');
    });
    it('throws error if string to chop is not a string', () => {
      expect(() => Alfred.chop({})).toThrow('Can only chop strings');
    });
    it('throws error if string to chopBy is undefined', () => {
      expect(() => Alfred.chop('chop.this')).toThrow('Please specify where to chop this');
    });
    it('throws error if string to chopBy is undefined', () => {
      expect(() => Alfred.chop('chop.this', {})).toThrow('Can only chop by a string');
    });
    it('throws error if keys is not an array', () => {
      expect(() => Alfred.chop('chop.this', '.', {
        keyOne: 'one',
        keyTwo: 'two',
      })).toThrow('They keys (third param) needs to be an array');
    });

    it('return an array of chopped items if keys not provided', () => {
      const expectedResult = ['chop', 'this'];
      const result = Alfred.chop('chop.this', '.');

      expect(result).toEqual(expectedResult);
    });
    it('return an object of chopped items with keys from keys array', () => {
      const expectedResult = {
        action: 'chop',
        subject: 'this',
      };
      const keys = ['action', 'subject'];

      const result = Alfred.chop('chop.this', '.', keys);
      expect(result).toEqual(expectedResult);
    });
    it('ignores extra values', () => {
      const expectedResult = {
        action: 'chop',
        subject: 'this',
      };
      const keys = ['action', 'subject'];

      const result = Alfred.chop('chop.this.now', '.', keys);
      expect(result).toEqual(expectedResult);
    });
    it('set value as undefined for extra keys', () => {
      const expectedResult = {
        action: 'chop',
        subject: 'this',
        time: undefined,
      };
      const keys = ['action', 'subject', 'time'];

      const result = Alfred.chop('chop.this', '.', keys);
      expect(result).toEqual(expectedResult);
    });
  });
  describe('clean', () => {
    it('throws error if parameter is not an array or an object', () => {
      expect(() => Alfred.clean('this is dirty')).toThrow('Clean can only clean Arrays or Objects. You sent string to clean');
    });
    it('cleans array of primitive values', () => {
      const toClean = ['this', 'that', undefined, false, 'that again', 1, 3];
      const cleaned = ['this', 'that', 'that again', 1, 3];
      expect(Alfred.clean(toClean)).toEqual(cleaned);
    });
    it('cleans array of objects', () => {
      const toClean = [{
        id: 1,
      }, {
        id: 2,
      },
      {},
      {
        id: undefined,
      },
      {
        id: 3,
      }];
      const cleaned = [{
        id: 1,
      }, {
        id: 2,
      }, {
        id: 3,
      }];
      expect(Alfred.clean(toClean)).toEqual(cleaned);
    });
    it('cleans array with both primitive and objects', () => {
      const toClean = [
        'this',
        'that',
        undefined,
        {
          id: 1,
        },
        1,
        null,
      ];
      const cleaned = ['this', 'that', { id: 1 }, 1];
      expect(Alfred.clean(toClean)).toEqual(cleaned);
    });
    it('cleans objects [undefined]', () => {
      const toClean = {
        id: 1,
        name: 2,
        class: undefined,
        pop: 3,
      };

      const cleaned = {
        id: 1,
        name: 2,
        pop: 3,
      };
      expect(Alfred.clean(toClean)).toEqual(cleaned);
    });
    it('cleans objects [null]', () => {
      const toClean = {
        id: 1,
        name: 2,
        class: null,
        pop: 3,
      };

      const cleaned = {
        id: 1,
        name: 2,
        pop: 3,
      };
      expect(Alfred.clean(toClean)).toEqual(cleaned);
    });
    it('cleans object where key value is an empty string', () => {
      const toClean = {
        name: 'Bob',
        profession: '',
      };
      const expectedCleaned = {
        name: 'Bob',
      };
      expect(Alfred.clean(toClean, true)).toEqual(expectedCleaned);
    });
    it('cleans sub objects', () => {
      const toClean = {
        name: 'Bob',
        address: {
          city: 'Bangkok',
          street: '',
        },
      };
      const expectedClean = {
        name: 'Bob',
        address: {
          city: 'Bangkok',
        },
      };

      expect(Alfred.clean(toClean, true)).toEqual(expectedClean);
    });
  });
  describe('arrayContains', () => {
    it('returns true is super array contains sub array', () => {
      const superArray = [1, 2, 3, 4, 5];
      const subArray = [2, 4, 5];

      expect(Alfred.arrayContains(superArray, subArray)).toBe(true);
    });
    it('returns false is super array does not contain sub array', () => {
      const superArray = [1, 2, 3, 4, 5];
      const subArray = [2, 4, 8];

      expect(Alfred.arrayContains(superArray, subArray)).toBe(false);
    });
  });
  describe('simplifyRDData', () => {
    it('throws error if rdData not given', () => {
      expect(() => Alfred.simplifyRDData()).toThrow('Cannot simplify rdData because rdData is not valid');
    });
    it('throws error is rdData is not an object', () => {
      expect(() => Alfred.simplifyRDData('rdData')).toThrow('rdData is not an object');
      expect(() => Alfred.simplifyRDData(2)).toThrow('rdData is not an object');
      expect(() => Alfred.simplifyRDData([{
        rdData: {},
      }])).toThrow('rdData is not an object');
    });
    it('throws error if rdData does not have info', () => {
      const rdData = {
        id: 1,
      };
      expect(() => Alfred.simplifyRDData(rdData)).toThrow('rdData.info should be an object but is undefined or null');
    });
    it('throws error if rdData.info is not an object', () => {
      const numberRdData = {
        id: 1,
        info: [],
      };
      expect(() => Alfred.simplifyRDData(numberRdData)).toThrow('rdData.info is not an object');
    });
    it('returns simplified rddata', () => {
      const rdData = {
        id: 1,
        info: {
          branch: {
            field_value: 'rama-3',
          },
          id: {
            field_value: '123',
          },
          department: {
            field_value: 'operations',
          },
        },
      };
      const expectedRdData = {
        id: 1,
        info: {
          branch: 'rama-3',
          department: 'operations',
          id: '123',
        },
      };

      expect(Alfred.simplifyRDData(rdData)).toEqual(expectedRdData);
    });
  });
  describe('replacePlaceholders', () => {
    it('returns empty string if template is undefined', () => {
      expect(Alfred.replacePlaceholders()).toEqual('');
    });
    it('returns empty string if template is null', () => {
      expect(Alfred.replacePlaceholders(null)).toEqual('');
    });
    it('replaces with empty string if replaceWith not specified', () => {
      expect(Alfred.replacePlaceholders('Hey this is {{name}}')).toEqual('Hey this is ');
    });
    it('replaces with replaceWith if specified', () => {
      expect(Alfred.replacePlaceholders('Hey this is {{name}}', '-')).toEqual('Hey this is -');
    });
  });
  describe('convertToConditionObject', () => {
    it('convert simple condition to object', () => {
      const conditionString = '[field] = 1';
      const expectedConditionObject = {
        field: {
          $eq: '1',
        },
      };

      expect(Alfred.convertToConditionObject(conditionString)).toEqual(expectedConditionObject);
    });
    it('convert single complex condition to object', () => {
      const conditionString = '[field] = 1 and [field-2] = 2';
      const expectedConditionObject = {
        $and: [
          {
            field: {
              $eq: '1',
            },
          },
          {
            'field-2': {
              $eq: '2',
            },
          },
        ],
      };

      expect(Alfred.convertToConditionObject(conditionString)).toEqual(expectedConditionObject);
    });
  });
  describe('evaluateCondition', () => {
    it('evaluates bool values correctly ', async () => {
      const conditionString = 'likes';
      const context = {
        likes: true,
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(true);
    });
    it('evaluates correctly', async () => {
      const conditionString = 'nps < 7 or nps > 8 ';
      const context = {
        nps: 9,
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(true);
    });
    it('evaluates string correctly', async () => {
      const conditionString = 'channel = client.stress';
      const context = {
        channel: 'stress',
        client: {
          stress: 'stress',
        },
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(true);
    });
    it('evaluates undefined condition', async () => {
      const conditionString = 'resolve != undefined and resolve != 1';
      const context = {
        channel: 'stress',
        client: {
          stress: 'stress',
        },
        resolve: 2,
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(true);
    });
    it('evaluates not equal condition correctly', async () => {
      const conditionString = 'channel != client.stress';
      const context = {
        channel: 'stress',
        client: {
          stress: 'happy',
        },
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(true);
    });
    it('evaluates not equal condition correctly', async () => {
      const conditionString = 'survey.csat < 8 and survey.csat != undefined and survey.csat != ""';
      const context = {
        channel: 'stress',
        survey: {
        },
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(false);
    });
    it('Can evaluate condition by comparing sub object key in array', async () => {
      const conditionString = 'survey.multi[.first = "b"]|exists';
      const context = {
        channel: 'stress',
        survey: {
          multi: [{
            first: 'a',
            last: 'b',
          }, {
            first: 'b',
            last: 'c',
          }],
        },
      };

      expect.assertions(1);

      expect(await Alfred.evaluateCondition(conditionString, context)).toEqual(true);
    });
  });
  describe('jexlify', () => {
    it('does not replace OR if OR is part of another word', () => {
      const condition = 'bla = blora or bla = blandra';
      const expectedCondition = 'bla == blora || bla == blandra';

      expect(Alfred.jexlify(condition)).toEqual(expectedCondition);
    });

    it('does not replace and,AND,or,OR inside single quote, double quote string', () => {
      const condition = `a = 'a and b' and A = 'A AND B' AND a = 'a or b' OR A = "A OR B"`;
      const expectedCondition = `a == 'a and b' && A == 'A AND B' && a == 'a or b' || A == "A OR B"`;

      expect(Alfred.jexlify(condition)).toEqual(expectedCondition);
    });
  });
  describe('hydrateString', () => {
    it('returns string from template and content', () => {
      const template = 'Hi {{name}}, Nice to meet you';
      const user = {
        name: 'Bob',
      };

      const expectedString = 'Hi Bob, Nice to meet you';

      expect(Alfred.hydrateString(template, user)).toEqual(expectedString);
    });
    it('remove curly braces if value is missing in content', () => {
      const template = 'Hi {{name}}, Nice to meet you';
      const user = {
        firstName: 'Bob',
        lastName: 'Mob',
      };

      const expectedString = 'Hi name, Nice to meet you';

      expect(Alfred.hydrateString(template, user)).toEqual(expectedString);
    });
    it('formats date if template syntax matches', () => {
      const template = 'Hi {{name}}, You were born on {{birthday|DD/MM/YYYY hh:mm}} and birthday is {{transactiondate|DD-MM-YYYY}}';
      const user = {
        name: 'Bob',
        birthday: '2018-05-06T14:07:28.000Z',
        transactiondate: '2018-04-07 00:00:00',
      };

      const expectedString = 'Hi Bob, You were born on 06/05/2018 09:07 and birthday is 07-04-2018';

      expect(Alfred.hydrateString(template, user)).toEqual(expectedString);
    });
    it('formats date if template syntax matches from sub object', () => {
      const template = 'Hi {{name}}, You were born on {{birthday|DD/MM/YYYY hh:mm}} and birthday is {{meta.transactiondate|DD-MM-YYYY}}';
      const user = {
        name: 'Bob',
        birthday: '2018-05-06T14:07:28.000Z',
        meta: {
          transactiondate: '2018-04-07 00:00:00',
        },
      };

      const expectedString = 'Hi Bob, You were born on 06/05/2018 09:07 and birthday is 07-04-2018';

      expect(Alfred.hydrateString(template, user)).toEqual(expectedString);
    });
    it('replaces date template with placeholder if data not found', () => {
      const template = 'Hi {{name}}, You were born on {{birthday|DD/MM/YYYY hh:mm}} and birthday is {{meta.transactiondate|DD-MM-YYYY}}';
      const user = {
        name: 'Bob',
        birthday: '2018-05-06T14:07:28.000Z',
        meta: {
          transactiondate2: '2018-04-07 00:00:00',
        },
      };

      const expectedString = 'Hi Bob, You were born on 06/05/2018 09:07 and birthday is meta.transactiondate';

      expect(Alfred.hydrateString(template, user, true)).toEqual(expectedString);
    });
    it('replaces date template with empty space if data not found and replaceUnfoundDataWithVariableName = false', () => {
      const template = 'Hi {{name}}, You were born on {{birthday|DD/MM/YYYY hh:mm}} and birthday is {{meta.transactiondate|DD-MM-YYYY}}';
      const user = {
        name: 'Bob',
        birthday: '2018-05-06T14:07:28.000Z',
        meta: {
          transactiondate2: '2018-04-07 00:00:00',
        },
      };

      const expectedString = 'Hi Bob, You were born on 06/05/2018 09:07 and birthday is ';

      expect(Alfred.hydrateString(template, user, false)).toEqual(expectedString);
    });
    it('does not replaces date template if data not found and replaceUnfoundDataWithVariableName = "strict"', () => {
      const template = 'Hi {{name}}, You were born on {{birthday|DD/MM/YYYY hh:mm}} and birthday is {{meta.transactiondate|DD-MM-YYYY}}';
      const user = {
        name: 'Bob',
        birthday: '2018-05-06T14:07:28.000Z',
        meta: {
          transactiondate2: '2018-04-07 00:00:00',
        },
      };

      const expectedString = 'Hi Bob, You were born on 06/05/2018 09:07 and birthday is {{meta.transactiondate|DD-MM-YYYY}}';

      expect(Alfred.hydrateString(template, user, 'strict')).toEqual(expectedString);
    });
    it('replaces placeholder with data from sub object', () => {
      const template = 'Hi {{user.name}}, Nice to meet you';
      const context = {
        user: {
          name: 'Bob',
        },
      };

      const expectedString = 'Hi Bob, Nice to meet you';

      expect(Alfred.hydrateString(template, context)).toEqual(expectedString);
    });
    it('replaces unfound data with variable name by default', () => {
      const template = 'So I am a {{person}}';
      const context = {
        occupation: 1,
      };
      const expectedString = 'So I am a person';

      expect(Alfred.hydrateString(template, context)).toEqual(expectedString);
    });
    it('replaces unfound data with blank string if replaceUnfoundDataWithVariableName is specified to false', () => {
      const template = 'So I am a {{person}}';
      const context = {
        occupation: 1,
      };
      const expectedString = 'So I am a ';

      expect(Alfred.hydrateString(template, context, false)).toEqual(expectedString);
    });
    it('replaces data with mapping if mapping object sent', () => {
      const template = 'So I am a {{occupation}}';
      const context = {
        occupation: 1,
      };
      const contextMapping = {
        occupation: {
          1: 'Salesman',
        },
      };
      const expectedString = 'So I am a Salesman';

      expect(Alfred.hydrateString(
        template,
        context,
        undefined,
        contextMapping,
      )).toEqual(expectedString);
    });
    it('replaces data with default in mapping if mapping object does not have data', () => {
      const template = 'So I am a {{occupation}}';
      const context = {
        occupation: 2,
      };
      const contextMapping = {
        occupation: {
          1: 'Salesman',
          default: 'Human',
        },
      };
      const expectedString = 'So I am a Human';

      expect(Alfred.hydrateString(
        template,
        context,
        undefined,
        contextMapping,
      )).toEqual(expectedString);
    });
    it('replaces data with empty string if mapping object does not have data and default', () => {
      const template = 'So I am a {{occupation}}';
      const context = {
        occupation: 2,
      };
      const contextMapping = {
        occupation: {
          1: 'Salesman',
        },
      };
      const expectedString = 'So I am a ';

      expect(Alfred.hydrateString(
        template,
        context,
        undefined,
        contextMapping,
      )).toEqual(expectedString);
    });
    it('returns original string if there is nothing to be hydrated', () => {
      const template = 'I have no vars';
      const context = {};

      expect(Alfred.hydrateString(template, context)).toEqual(template);
    });
    it('returns distinct values only on distinct modifier', () => {
      const template = '{{users.name|distinct}}';
      const context = {
        users: [{
          name: 'James',
        }, {
          name: 'James',
        }, {
          name: 'Bob',
        }],
        name: 'FOO BAR',
        nps: 5,
      };
      const expected = 'James,Bob';

      expect(Alfred.hydrateString(template, context)).toEqual(expected);
    });
  });
  describe('hydrateObject', () => {
    it('throws error if template is not an object', () => {
      expect(() => Alfred.hydrateObject()).toThrow('Object template should be an object');
    });
    it('throws error if context is not an object', () => {
      expect(() => Alfred.hydrateObject({})).toThrow('Context should be an object');
    });
    it('defaults context key to context', () => {
      const template = {
        survey: '{{context}}',
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
      };
      // const key = 'context';
      const expected = {
        survey: context,
      };

      expect(Alfred.hydrateObject(template, context)).toEqual(expected);
    });
    it('Replaces data from sub object', () => {
      const template = {
        survey: '{{context.survey}}',
      };
      const survey = {
        id: 1,
        csat: 2,
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
        survey,
      };
      // const key = 'context';
      const expected = {
        survey,
      };

      expect(Alfred.hydrateObject(template, context)).toEqual(expected);
    });
    it('Replaces placeholder with context object', () => {
      const template = {
        survey: '{{context}}',
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
      };
      const key = 'context';
      const expected = {
        survey: context,
      };

      expect(Alfred.hydrateObject(template, context, key)).toEqual(expected);
    });
    it('Replaces placeholder with context object for dates too', () => {
      const template = {
        survey: '{{context}}',
        td: '{{context.transactiondate.td|DD/MM/YYYY}}',
        td1: '{{context.td|DD/MM/YYYY}}',
        td2: '{{context.transactiondate.td2.td|DD/MM/YYYY}}',
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
        transactiondate: {
          td: '2018-04-07 00:00:00',
          td2: {
            td: '2018-04-07 00:00:00',
          },
        },
        td: '2018-04-07 00:00:00',
      };
      const key = 'context';
      const expected = {
        survey: context,
        td: '07/04/2018',
        td1: '07/04/2018',
        td2: '07/04/2018',
      };

      expect(Alfred.hydrateObject(template, context, key)).toEqual(expected);
    });
    it('Replaces data from sub object', () => {
      const template = {
        survey: {
          id: '{{context.id}}',
          name: '{{context.name}}',
          nps: '{{context.nps}}',
        },
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
      };
      const key = 'context';
      const expected = {
        survey: context,
      };

      expect(Alfred.hydrateObject(template, context, key)).toEqual(expected);
    });
    it('Replaces data with undefined if sub object doesn\'t exist', () => {
      const template = {
        survey: {
          id: '{{context.id}}',
          name: '{{context.name}}',
          nps: '{{context.nps}}',
          user: '{{context.user.id}}',
        },
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
      };
      const key = 'context';
      const expected = {
        survey: context,
      };

      expect(Alfred.hydrateObject(template, context, key)).toEqual(expected);
    });
    it('Replaces placeholder with empty string for template values if data not found', () => {
      const template = {
        survey: {
          id: '{{context.id}}',
          name: '{{context.name}}',
          nps: 'NPS is {{context.nps}}',
        },
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
      };
      const key = 'context';
      const expected = {
        survey: {
          id: 1,
          name: 'FOO BAR',
          nps: 'NPS is ',
        },
      };

      expect(Alfred.hydrateObject(template, context, key)).toEqual(expected);
    });
    it('Replaces placeholder with return value from function if placeholder is a function call', () => {
      const template = {
        survey: {
          id: '{{context.id}}',
          name: '{{context.name}}',
          nps: 'NPS is {{context.nps}}',
          time_in_system: '$fromNow(({{context.create_date}}, years)) years',
          time_to_expire: '$fromNow((2017-02-05T14:48:00.000Z, years))',
        },
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 7,
        create_date: '2017-02-05T14:48:00.000Z',
      };
      const diff = moment().diff(moment('2017-02-05', 'YYYY-MM-DD'), 'years');
      const key = 'context';
      const expected = {
        survey: {
          id: 1,
          name: 'FOO BAR',
          nps: 'NPS is 7',
          time_in_system: `${diff} years`,
          time_to_expire: `${diff}`,
        },
      };

      expect(Alfred.hydrateObject(template, context, key)).toEqual(expected);
    });
    it('Replaces placeholder in array', () => {
      const template = {
        bar: ['foo', '{{context.id}}'],
      };
      const context = {
        id: 1,
        name: 'FOO BAR',
        nps: 5,
      };
      const expected = {
        bar: ['foo', '1'],
      };

      expect(Alfred.hydrateObject(template, context)).toEqual(expected);
    });

    it('Replaces placeholder in object with speacial placeholder with prefix and suffix', () => {
      const template = {
        content: {
          subject: '[FullLoop][Escalation]Negative feedback received on {{case.meta.Date_Time_Meta1|DD/MM/YYYY}} from survey',
        },
      };
      const context = {
        case: {
          case_number: 'TK-00083',
          uniqueTransactionKey: 'context.survey.rd_id',
          created_by: 'SYSTEM',
          created_on: '2019-08-19T10:34:41.067Z',
          meta: {
            Location_Meta3: 'ABF',
            Customer_Meta2: 'Hotel',
            Location_Meta4: 'Kittisak Intacotchasarn',
            Location_Meta5: 'Grand Centre Point Hotel Pratunam',
            Date_Time_Meta1: '2019-08-19T05:33:03.000Z',
            Customer_Meta1: 'Hotel',
            Location_Meta1: 'Seefah Lumpini',
            Location_Meta2: 'Hotel',
          },
        },
      };

      const expected = {
        content: {
          subject: '[FullLoop][Escalation]Negative feedback received on 19/08/2019 from survey',
        },
      };

      expect(Alfred.hydrateObject(template, context)).toEqual(expected);
    });
    it('Replaces placeholder by mapping value from subobjects in array', () => {
      const template = {
        bar: '{{context.users.name}}',
      };
      const context = {
        id: 1,
        users: [{
          name: 'Scott',
        }, {
          name: 'James',
        }, {
          name: 'Bob',
        }],
      };
      const expected = {
        bar: 'Scott,James,Bob',
      };

      expect(Alfred.hydrateObject(template, context)).toEqual(expected);
    });
  });
  describe('valueForKey', () => {
    it('returns value from object for key', () => {
      const key = 'name';
      const obj = {
        name: 'Bob',
      };

      expect(Alfred.valueForKey(obj, key)).toEqual('Bob');
    });
    it('returns value from sub object for key', () => {
      const key = 'user.name';
      const obj = {
        user: {
          name: 'Bob',
        },
      };

      expect(Alfred.valueForKey(obj, key)).toEqual('Bob');
    });
    it('returns combined string for array of objects', () => {
      const key = 'users.name';
      const obj = {
        users: [{
          name: 'Bob',
        }, {
          name: 'James',
        }, {
          name: 'Scott',
        }],
      };

      expect(Alfred.valueForKey(obj, key)).toEqual('Bob,James,Scott');
    });
    it('returns combined string for array of objects', () => {
      const key = 'users.name';
      const obj = {
        users: [{
          name: 'Bob',
        }, {
          firstName: 'James',
        }, {
          name: 'Scott',
        }],
      };

      expect(Alfred.valueForKey(obj, key)).toEqual('Bob,Scott');
    });
  });
  describe('removeFields', () => {
    it('throws error if object is not an object', () => {
      expect(() => Alfred.removeFields()).toThrow('Cannot remove fields because object provided is not an object');
    });
    it('throws error if fields to remove is not an array', () => {
      expect(() => Alfred.removeFields({}, 'a')).toThrow('Expected fields to remove to be an array of strings');
    });
    it('removes keys specified', () => {
      const obj = {
        a: '12',
        b: '21',
        c: '123',
        d: '321',
      };
      const fieldsToRemove = ['d'];
      const expectedObj = {
        a: '12',
        b: '21',
        c: '123',
      };
      expect(Alfred.removeFields(obj, fieldsToRemove)).toEqual(expectedObj);
    });
    it('removes keys from nested object', () => {
      const obj = {
        a: '12',
        b: '21',
        c: {
          d: '123',
          e: '321',
        },
      };
      const fieldsToRemove = ['c.e'];
      const expectedObj = {
        a: '12',
        b: '21',
        c: {
          d: '123',
        },
      };
      expect(Alfred.removeFields(obj, fieldsToRemove)).toEqual(expectedObj);
    });
  });
  describe('mapDefinitionValueToContext', () => {
    it('throws error if context is undefined', () => {
      expect(() => Alfred.mapDefinitionValueToContext()).toThrow('Context should not be undefined');
    });
    it('throws error if context is not an object', () => {
      expect(() => Alfred.mapDefinitionValueToContext('context')).toThrow('Context should be an object');
    });
    it('throws error if definitions is defined but not an array', () => {
      const context = {
        foo: 'bar',
      };
      expect(() => Alfred.mapDefinitionValueToContext(context, 'definitions')).toThrow('Definitions should be an array');
    });
    it('returns context if definitions is undefined', () => {
      const context = {
        foo: 'bar',
      };
      expect(Alfred.mapDefinitionValueToContext(context)).toEqual(context);
    });
    it('returns context if definitions is an empty array', () => {
      const context = {
        foo: 'bar',
      };
      expect(Alfred.mapDefinitionValueToContext(context)).toEqual(context);
    });
    it('maps value in context from definition', () => {
      const context = {
        Customer_Meta1: '1',
        Customer_Meta2: '2',
      };
      const definitions = [
        {
          name: 'tier',
          variable: 'Customer_Meta1',
          description: 'Customer tier',
          productIndex: '0',
          isMeta: 'T',
          mapping: [
            {
              name: '1',
              description: 'Free',
            },
            {
              name: '2',
              description: 'Starter',
            },
            {
              name: '3',
              description: 'SME',
            },
            {
              name: '4',
              description: 'Enterprise',
            },
            {
              name: '5',
              description: 'Internal',
            },
          ],
        },
        {
          name: 'segment',
          variable: 'Customer_Meta2',
          description: 'Customer Segment',
          productIndex: '0',
          isMeta: 'T',
          mapping: [
            {
              name: '1',
              description: 'VIP',
            },
            {
              name: '2',
              description: 'High',
            },
            {
              name: '3',
              description: 'Medium',
            },
            {
              name: '4',
              description: 'Low',
            },
            {
              name: '5',
              description: 'Very Low',
            },
          ],
        },
      ];

      const expected = {
        Customer_Meta1: 'Free',
        Customer_Meta2: 'High',
      };

      expect(Alfred.mapDefinitionValueToContext(context, definitions)).toEqual(expected);
    });
    it('returns unmapped value if definition not found', () => {
      const context = {
        Customer_Meta1: '1',
        Customer_Meta2: '2',
        Location_Meta1: '2',
      };
      const definitions = [
        {
          name: 'tier',
          variable: 'Customer_Meta1',
          description: 'Customer tier',
          productIndex: '0',
          isMeta: 'T',
          mapping: [
            {
              name: '1',
              description: 'Free',
            },
            {
              name: '2',
              description: 'Starter',
            },
            {
              name: '3',
              description: 'SME',
            },
            {
              name: '4',
              description: 'Enterprise',
            },
            {
              name: '5',
              description: 'Internal',
            },
          ],
        },
        {
          name: 'segment',
          variable: 'Customer_Meta2',
          description: 'Customer Segment',
          productIndex: '0',
          isMeta: 'T',
          mapping: [
            {
              name: '1',
              description: 'VIP',
            },
            {
              name: '2',
              description: 'High',
            },
            {
              name: '3',
              description: 'Medium',
            },
            {
              name: '4',
              description: 'Low',
            },
            {
              name: '5',
              description: 'Very Low',
            },
          ],
        },
      ];

      const expected = {
        Customer_Meta1: 'Free',
        Customer_Meta2: 'High',
        Location_Meta1: '2',
      };

      expect(Alfred.mapDefinitionValueToContext(context, definitions)).toEqual(expected);
    });
  });
  describe('realTypeof', () => {
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf('blabla')).toBe('string');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf(1)).toBe('number');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf(true)).toBe('boolean');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf(() => 1)).toBe('function');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf({
        foo: 'bar',
      })).toBe('object');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf([])).toBe('array');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf(undefined)).toBe('undefined');
    });
    it('returns type as string for string', () => {
      expect(Alfred.realTypeOf(null)).toBe('null');
    });
  });
  describe('mapValues', () => {
    it('throws error if object is not an object', () => {
      expect(() => Alfred.mapValues()).toThrow('Expected object to be an object or an array, instead received undefined');
    });
    it('throws error if context is not an object', () => {
      expect(() => Alfred.mapValues({})).toThrow('Expected context to be an object, instead received undefined');
    });
    it('returns object if context is an empty object', () => {
      const object = {
        username: 'John',
      };

      expect(Alfred.mapValues(object, {})).toEqual(object);
    });
    it('returns object if context does not have matching data', () => {
      const object = {
        username: 'John',
      };
      const context = {
        Bob: 'bob@bobby.bobber',
      };

      expect(Alfred.mapValues(object, context)).toEqual(object);
    });
    it('returns mapped object if context has matching data', () => {
      const object = {
        username: 'John',
      };
      const context = {
        John: 'johnyboy2021',
      };
      const expected = {
        username: 'johnyboy2021',
      };

      expect(Alfred.mapValues(object, context)).toEqual(expected);
    });
    it('returns array with mapped value if context has matching data', () => {
      const object = ['John', 'Bob'];
      const context = {
        John: 'johnyboy2021',
        Bob: 'bobbybobblob',
      };
      const expected = ['johnyboy2021', 'bobbybobblob'];

      expect(Alfred.mapValues(object, context)).toEqual(expected);
    });
  });
  describe('evaluate', () => {
    it('replaces value for placeholder templates', () => {
      const template = '{{name}} is 28 years old';
      const context = {
        name: 'Foo Bar',
        age: 28,
      };

      const expected = 'Foo Bar is 28 years old';

      expect(Alfred.evaluate(template, context)).toEqual(expected);
    });
    it('returns template if template does not have placeholders', () => {
      const template = 'Foo Bar is 27 years old';

      const context = {};

      expect(Alfred.evaluate(template, context)).toEqual(template);
    });
    it('replaces function with empty string if it includes unsupported function', () => {
      const template = 'Foo Bar is $from(({{birthDate}},{{today}}, years)) years old';

      const context = {
        birthDate: '1991-02-05T14:48:00.000Z',
        today: '2019-02-05T14:48:00.000Z',
      };

      const expected = 'Foo Bar is  years old';

      expect(Alfred.evaluate(template, context)).toEqual(expected);
    });
    describe('$fromNow', () => {
      it('replaces value for function templates', () => {
        const template = '{{name}} is $fromNow(({{birthDate}}, {{unit}})) years old';
        const context = {
          name: 'Foo Bar',
          birthDate: '1991-02-05T14:48:00.000Z',
          unit: 'years',
        };

        const diff = moment().diff(moment('1991-02-05', 'YYYY-MM-DD'), 'years');

        const expected = `Foo Bar is ${diff} years old`;

        expect(Alfred.evaluate(template, context)).toEqual(expected);
      });
    });
    describe('$today', () => {
      it('replaces value for function templates', () => {
        const template = "<!DOCTYPE html><html lang='en'><head> <title>[FullLoopCX][Escalation]Negative Feedback Summary $today((DD/MM/YYYY))</title> <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/> <meta name='viewport' content='width=device-width, initial-scale=1'> <meta http-equiv='X-UA-Compatible' content='IE=edge'/> <style type='text/css'> /* RESET STYLES */ img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}table{border-collapse: collapse !important;}body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; font: sans-serif;}</style></head><body style='background-color: white; margin: 0 !important; padding: 10px 10px 60px 10px !important;'> <table border='0' cellspacing='0' cellpadding='0' role='presentation' width='100%'> <tr> <td style='font-family: sans-serif; font-size: 18px; line-height: 28px; font-weight: bold'>[FullLoopCX][Escalation]Negative Feedback Summary</td></tr></table> <br/> <table border='0' cellspacing='0' cellpadding='0' role='presentation' width='100%'> <tr> <td style='font-family: sans-serif; font-size: 14px;'> เคสของคุณยังไม่ถูกปิดภายใน 72 ชั่วโมง </td></tr><tr> <td style='font-family: sans-serif; font-size: 14px; color: red; font-weight: bold'> กรุณาติดตามเรื่อง ตามรายละเอียดด้านล่างนี้ </td></tr></table> <br/> <table border='0' cellspacing='0' cellpadding='0' role='presentation' width='100%'> <tr> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Transaction ID </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Transaction Date </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Branch Area </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Branch Name </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Sales Name </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> CSAT Score </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Time Over SLA </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Link </th> </tr></table> <br/> <table> <tbody> <tr> <td style='font-family: sans-serif; font-size: 14px;'> คุณสามารถปิดเคสหรืออัพเดทเกี่ยวกับเคสนี้ จากลิ้งค์ด้านล่าง</td></tr><tr style='height: 20px'> </tr><tr> <td style='font-family: sans-serif; font-size: 14px;'> Best Regards, </td></tr><tr> <td style='font-family: sans-serif; font-size: 14px;'> <b>Voice of Customer team</b> </td></tr></tbody> </table></body></html>";
        const context = {
          name: 'Foo Bar',
          birthDate: '1991-02-05T14:48:00.000Z',
          unit: 'years',
        };
        const today = moment().format('DD/MM/YYYY');

        const expected = `<!DOCTYPE html><html lang='en'><head> <title>[FullLoopCX][Escalation]Negative Feedback Summary ${today}</title> <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/> <meta name='viewport' content='width=device-width, initial-scale=1'> <meta http-equiv='X-UA-Compatible' content='IE=edge'/> <style type='text/css'> /* RESET STYLES */ img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}table{border-collapse: collapse !important;}body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; font: sans-serif;}</style></head><body style='background-color: white; margin: 0 !important; padding: 10px 10px 60px 10px !important;'> <table border='0' cellspacing='0' cellpadding='0' role='presentation' width='100%'> <tr> <td style='font-family: sans-serif; font-size: 18px; line-height: 28px; font-weight: bold'>[FullLoopCX][Escalation]Negative Feedback Summary</td></tr></table> <br/> <table border='0' cellspacing='0' cellpadding='0' role='presentation' width='100%'> <tr> <td style='font-family: sans-serif; font-size: 14px;'> เคสของคุณยังไม่ถูกปิดภายใน 72 ชั่วโมง </td></tr><tr> <td style='font-family: sans-serif; font-size: 14px; color: red; font-weight: bold'> กรุณาติดตามเรื่อง ตามรายละเอียดด้านล่างนี้ </td></tr></table> <br/> <table border='0' cellspacing='0' cellpadding='0' role='presentation' width='100%'> <tr> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Transaction ID </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Transaction Date </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Branch Area </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Branch Name </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Sales Name </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> CSAT Score </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Time Over SLA </th> <th style='font-family: sans-serif; font-size: 14px; text-align: left;'> Link </th> </tr></table> <br/> <table> <tbody> <tr> <td style='font-family: sans-serif; font-size: 14px;'> คุณสามารถปิดเคสหรืออัพเดทเกี่ยวกับเคสนี้ จากลิ้งค์ด้านล่าง</td></tr><tr style='height: 20px'> </tr><tr> <td style='font-family: sans-serif; font-size: 14px;'> Best Regards, </td></tr><tr> <td style='font-family: sans-serif; font-size: 14px;'> <b>Voice of Customer team</b> </td></tr></tbody> </table></body></html>`;

        expect(Alfred.evaluate(template, context)).toEqual(expected);
      });
    });
  });
  describe('sleep', () => {
    it('sleeps based on time given', async () => {
      expect.assertions(1);
      const timeStart = new Date();
      const sleepForSeconds = 1.2;
      return Alfred.sleep(sleepForSeconds)
        .then(() => {
          const timeEnd = new Date();
          const totalSeconds = timeEnd.getTime() - timeStart.getTime();

          expect(totalSeconds >= sleepForSeconds).toBe(true);
        });
    });
  });
});

describe('hydrateTemplate', () => {
  it('throws error if template is undefined', () => {
    expect(() => Alfred.hydrateTemplate()).toThrow('Expected template to be a string, instead got undefined');
  });
  it('throws error if items is not an array', () => {
    expect(() => Alfred.hydrateTemplate('', {})).toThrow('Expected items to be an array or a json string, instead got object');
  });
  it('returns empty string if items is an empty array', () => {
    expect(Alfred.hydrateTemplate('', [])).toEqual('');
  });
  it('returns an array of hydrated template string', () => {
    const template = '{{name}} is a good {{gender}}';
    const items = [
      {
        name: 'Bob',
        gender: 'boy',
      },
      {
        name: 'John',
        gender: 'boy',
      },
      {
        name: 'Jane',
        gender: 'girl',
      },
      {
        name: 'Annabelle',
        gender: 'girl',
      },
    ];

    const expected = [
      'Bob is a good boy',
      'John is a good boy',
      'Jane is a good girl',
      'Annabelle is a good girl',
    ];
    expect(Alfred.hydrateTemplate(template, items)).toEqual(expected);
  });
  it('returns a string consisting of all hydrated template string', () => {
    const template = '{{name}} is a good {{gender}}';
    const items = [
      {
        name: 'Bob',
        gender: 'boy',
      },
      {
        name: 'John',
        gender: 'boy',
      },
      {
        name: 'Jane',
        gender: 'girl',
      },
      {
        name: 'Annabelle',
        gender: 'girl',
      },
    ];

    const expected = [
      'Bob is a good boy',
      'John is a good boy',
      'Jane is a good girl',
      'Annabelle is a good girl',
    ].join('');
    expect(Alfred.hydrateTemplate(template, items, true)).toEqual(expected);
  });
  it('returns a string consisting of all hydrated template string for JSON stringified items', () => {
    const template = '{{name}} is a good {{gender}}';
    const items = [
      {
        name: 'Bob',
        gender: 'boy',
      },
      {
        name: 'John',
        gender: 'boy',
      },
      {
        name: 'Jane',
        gender: 'girl',
      },
      {
        name: 'Annabelle',
        gender: 'girl',
      },
    ];

    const expected = [
      'Bob is a good boy',
      'John is a good boy',
      'Jane is a good girl',
      'Annabelle is a good girl',
    ].join('');
    expect(Alfred.hydrateTemplate(template, JSON.stringify(items), true)).toEqual(expected);
  });
  it('returns a string consisting of all hydrated template string joined by provided param', () => {
    const template = '{{name}} is a good {{gender}}';
    const items = [
      {
        name: 'Bob',
        gender: 'boy',
      },
      {
        name: 'John',
        gender: 'boy',
      },
      {
        name: 'Jane',
        gender: 'girl',
      },
      {
        name: 'Annabelle',
        gender: 'girl',
      },
    ];

    const expected = [
      'Bob is a good boy',
      'John is a good boy',
      'Jane is a good girl',
      'Annabelle is a good girl',
    ].join(' and ');
    expect(Alfred.hydrateTemplate(template, items, true, ' and ')).toEqual(expected);
  });
  it('works with HTML template', () => {
    const itemTemplate = '<tr><td>{{name}}</td><td>{{gender}}</td></tr>';
    const items = [
      {
        name: 'Bob',
        gender: 'boy',
      },
      {
        name: 'John',
        gender: 'boy',
      },
      {
        name: 'Jane',
        gender: 'girl',
      },
      {
        name: 'Annabelle',
        gender: 'girl',
      },
    ];

    const expected = '<tr><td>Bob</td><td>boy</td></tr><tr><td>John</td><td>boy</td></tr><tr><td>Jane</td><td>girl</td></tr><tr><td>Annabelle</td><td>girl</td></tr>';

    expect(Alfred.hydrateTemplate(itemTemplate, items, true, '', false)).toEqual(expected);
  });
  it('returns array of parameters', () => {
    const parameterString = '(foo, bar, baz)';
    const expected = [
      'foo',
      'bar',
      'baz',
    ];

    expect(Alfred.getParametersFromString(parameterString)).toEqual(expected);
  });
  it('returns array of parameters with object as param', () => {
    const obj = {
      foo: 'bar',
      bar: 'foo',
    };
    const objJSONString = JSON.stringify(obj);
    const parameterString = `(foo, ${objJSONString}, baz)`;
    const expected = [
      'foo',
      objJSONString,
      'baz',
    ];

    expect(Alfred.getParametersFromString(parameterString)).toEqual(expected);
  });
  it('throw error when does not send periodDuration parameter', () => {
    expect(Alfred.getDateByPeriodTime).toThrowError('periodDuration must be string');
  });
  it('throw error when from parameter is not moment or string', () => {
    expect(() => Alfred.getDateByPeriodTime('1M', {})).toThrowError('from must be string');
  });
  it('throw error when does not send fromFormat parameter', () => {
    expect(() => Alfred.getDateByPeriodTime('1M', '2018-01-01')).toThrowError('fromFormat must be string');
  });
  it('throw error when from direction parameter is not match', () => {
    expect(() => Alfred.getDateByPeriodTime('1M', '2018-01-01', 'YYYY-MM-DD', {})).toThrowError('direction must be string');
  });
  it('throw error when from direction parameter is not match', () => {
    expect(() => Alfred.getDateByPeriodTime('1M', '2018-01-01', 'YYYY-MM-DD', '*')).toThrowError('direction is not match');
  });
  it('return date previous 1 month from today', () => {
    const time = Alfred.getDateByPeriodTime('1M');
    const previous1M = moment().utc().endOf('date').subtract('1', 'M').toISOString();
    expect(time).toEqual(previous1M);
  });
  it('return date previous 2 months from today', () => {
    const time = Alfred.getDateByPeriodTime('2M');
    const previous2M = moment().utc().endOf('date').subtract('2', 'M').toISOString();
    expect(time).toEqual(previous2M);
  });
  it('return date previous 1 day from today', () => {
    const time = Alfred.getDateByPeriodTime('1d');
    const previous1d = moment().utc().endOf('date').subtract('1', 'd').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 7 days from today', () => {
    const time = Alfred.getDateByPeriodTime('7d');
    const previous1d = moment().utc().endOf('date').subtract('7', 'd').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 1 quarter from today', () => {
    const time = Alfred.getDateByPeriodTime('1q');
    const previous1d = moment().utc().endOf('date').subtract('1', 'Q').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 3 quarters from today', () => {
    const time = Alfred.getDateByPeriodTime('3q');
    const previous1d = moment().utc().endOf('date').subtract('3', 'Q').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 1 week from today', () => {
    const time = Alfred.getDateByPeriodTime('1w');
    const previous1d = moment().utc().endOf('date').subtract('1', 'w').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 4 weeks from today', () => {
    const time = Alfred.getDateByPeriodTime('4w');
    const previous1d = moment().utc().endOf('date').subtract('4', 'w').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 1 year from today', () => {
    const time = Alfred.getDateByPeriodTime('1y');
    const previous1d = moment().utc().endOf('date').subtract('1', 'y').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 4 years from today', () => {
    const time = Alfred.getDateByPeriodTime('4y');
    const previous1d = moment().utc().endOf('date').subtract('4', 'y').toISOString();
    expect(time).toEqual(previous1d);
  });
  it('return date previous 1 month from 2019-02-20', () => {
    const time = Alfred.getDateByPeriodTime('1M', '2019-02-20', 'YYYY-MM-DD');
    const previous1M = moment('2019-02-20', 'YYYY-MM-DD').utc().endOf('date').subtract('1', 'M').toISOString();
    expect(time).toEqual(previous1M);
  });
  it('return date after 1 month from 2019-02-20', () => {
    const time = Alfred.getDateByPeriodTime('1M', '2019-02-20', 'YYYY-MM-DD', Alfred.DIRECTION_PERIOD_TIME.AFTER);
    const previous1M = moment('2019-02-20', 'YYYY-MM-DD').utc().endOf('date').add('1', 'M').toISOString();
    expect(time).toEqual(previous1M);
  });

  it('correctly transforms dynamo db', () => {
    const dynamoItem = {
      modified_on: {
        S: '2018-05-17T08:01:45.749Z',
      },
      escalation_notifed: {
        BOOL: false,
      },
      latest_status_changed_on: {
        S: '2018-05-17T08:00:25.067Z',
      },
      source: {
        S: 'home',
      },
      title: {
        S: 'ABSGJDSNKL2819',
      },
      created_by: {
        S: 'SYSTEM',
      },
      client_id: {
        S: '53a40c1a-fc0c-41f4-9b87-86d19ecd6f3a',
      },
      transactionId: {
        S: 'ABSGJDSNKL2819',
      },
      survey_response: {
        M: {
          comments: {
            S: 'foo bar is a foo bar',
          },
          csat: {
            N: '1',
          },
          transactionId: {
            S: 'ABSGJDSNKL2819',
          },
        },
      },
      is_escalated: {
        BOOL: true,
      },
      created_on: {
        S: '2018-05-17T08:00:25.068Z',
      },
      deleted_flag: {
        S: 'N',
      },
      survey: {
        M: {
          comments: {
            S: 'foo bar is a foo bar',
          },
          csat: {
            N: '1',
          },
          transactionId: {
            S: 'ABSGJDSNKL2819',
          },
        }
      },
      case_number: {
        S: 'TK-00004',
      },
      id: {
        S: '0935512b-945d-492d-a311-05c76ad5b3e7',
      },
      c_status: {
        S: 'OPEN',
      },
      user: {
        M: {
          segment: {
            S: 'OWNER',
          },
          name: {
            S: 'John Doe',
          }
        }
      }
    };
    const expectedItem = {
      modified_on: '2018-05-17T08:01:45.749Z',
      escalation_notifed: false,
      latest_status_changed_on: '2018-05-17T08:00:25.067Z',
      source: 'home',
      title: 'ABSGJDSNKL2819',
      created_by: 'SYSTEM',
      client_id: '53a40c1a-fc0c-41f4-9b87-86d19ecd6f3a',
      transactionId: 'ABSGJDSNKL2819',
      survey_response: {
        comments: 'foo bar is a foo bar',
        csat: 1,
        transactionId: 'ABSGJDSNKL2819',
      },
      is_escalated: true,
      created_on: '2018-05-17T08:00:25.068Z',
      deleted_flag: 'N',
      survey: {
        comments: 'foo bar is a foo bar',
        csat: 1,
        transactionId: 'ABSGJDSNKL2819',
      },
      case_number: 'TK-00004',
      id: '0935512b-945d-492d-a311-05c76ad5b3e7',
      c_status: 'OPEN',
      user: {
        segment: 'OWNER',
        name: 'John Doe',
      },
    };
    expect(Alfred.dynamoItemToObj(dynamoItem)).toEqual(expectedItem);
  });
});

