import jexl from 'jexl';
import moment from 'moment';

import evalFunctions from './evalFunctions';

/**
 * Replaces value for key with representation in mapping object
 * @param  {String} key     The key
 * @param  {String} value   The value to be mapped
 * @param  {Object} mapping Mapping object with mapping for value
 * @return {String}         Mapped value
 */
export const replaceValueFromMapping = (key, value, mapping) => {
  let mappedValue = value;
  if (typeof mapping !== 'undefined') {
    if (typeof mapping[key] !== 'undefined') {
      mappedValue = mapping[key][value];
      if (typeof mappedValue === 'undefined') {
        mappedValue = mapping[key].default || '';
      }
    }
  }

  return mappedValue;
};
/**
 * Takes a param and checks if it's an object
 * @param  {any} obj    whatever you need to check
 * @return {bool}       whether the input is an object
 */
export const isObject = (obj) => {
  let isObj = true;
  if (!obj) {
    isObj = false;
  } else if (typeof obj !== 'object' || Array.isArray(obj)) {
    isObj = false;
  }

  return isObj;
};

export const DIRECTION_PERIOD_TIME = Object.freeze({
  AFTER: 'after',
  BEFORE: 'before',
});

/**
 * Counts the number of times a string occurs in a parent string
 * @param  {String} parentString The parent string
 * @param  {String} substring    The substring
 * @return {Number}              The number of times substring appears in parentString
 */
export const countSubstringInString = (parentString = '', subString = '') => {
  let count = 0;
  if (typeof parentString === 'string' && typeof subString === 'string') {
    count = parentString.split(subString).length - 1;
  } else {
    throw new Error('Parent String and sub string need to be a string');
  }

  return count;
};
/**
 * Will take a url and return just the base url
 * @param  {string} url The url you want to process
 * @return {string}     The base url or empty string if base url could not be extracted
 * @throws {error} If url is undefined
 * @throws {error} If url is not a string
 */
export const extractBaseUrl = (url) => {
  // const baseUrlPattern = /^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*?(:[0-9]+)?(\/)?$/g;

  if (!url) throw new Error('Cannot extract base url from undefined');
  if (typeof url !== 'string') throw new Error('Can only extract base url from string');

  // const baseUrlPattern = /^https?:\/\/[^\/:]+/i;
  const baseUrlPattern = /^(?:(ftp|http|https)?:\/\/)?[^\/:]+/i;
  const baseUrl = url.match(baseUrlPattern) || '';

  return baseUrl[0] || '';
};

/**
 * Will take a url and return the path. It's the opposite of extractBaseUrl()
 * @param  {string} url The url you want to process
 * @param {bool} removeFirstChar If true, it'll remove the first letter from the path
 * @return {string}     The path from the url
 * @throws {error} If url is undefined
 * @throws {error} If url is not a string
 */
export const extractPath = (url, removeFirstChar = false) => {
  if (!url) throw new Error('Cannot extract path from undefined');
  if (typeof url !== 'string') throw new Error('Can only extract path from string');

  const baseUrl = extractBaseUrl(url);
  let path = url.split(baseUrl)[1];

  if (removeFirstChar) {
    path = path.slice(1, path.length);
  }

  return path;
};

/**
 * Takes a string and an array of keys, chops the string and uses keys to create an object.
 * If no keys are provided, it'll return an array of chopped items. If there are more keys
 * than chopped items, the extra keys will map to undefined. If there are extra chopped items,
 * the extra ones will be ignored.
 * @param  {string} toChop     The string to chop
 * @param  {string} chopBy     The string that'll be used to decide where to chop.
 *                             Similar to how split('') works
 * @param  {array} [keys=[]]   An array of string, will be used to generate object
 * @return {object|array}      An array of chopped items or an object with chopped items as values
 */
export const chop = (toChop, chopBy, keys = []) => {
  if (!toChop) throw new Error('Cannot chop undefined');
  if (typeof toChop !== 'string') throw new Error('Can only chop strings');
  if (!chopBy) throw new Error('Please specify where to chop this');
  if (typeof chopBy !== 'string') throw new Error('Can only chop by a string');
  if (!Array.isArray(keys)) throw new Error('They keys (third param) needs to be an array');

  const choppedItems = keys.length > 0 ?
    toChop.split(chopBy, keys.length) :
    toChop.split(chopBy);
  if (keys.length === 0) return choppedItems;

  const packagedChopped = {};
  for (let i = keys.length - 1; i >= 0; i--) {
    const key = keys[i];
    packagedChopped[key] = choppedItems[i];
  }

  return packagedChopped;
};

/**
 * Returns value stored for a key. Works for nested objects.
 * Returns undefined if value does not exist
 * @param  {Object} object The object that holds the data
 * @param  {key} key       The key to fetch the data for. Supports
 *                         a.b.c.d
 * @return {any}           Returns value for the key
 */
export const valueForKey = (object, key) => {
  if (!isObject(object)) {
    throw new Error('Object is not an object');
  }
  if (!key && typeof key !== 'string') {
    throw new Error('key is undefined or not a string');
  }

  let value = '';
  if (key.includes('.')) {
    const parentKey = key.split('.')[0];
    const parent = object[parentKey];

    const childKey = key.replace(`${parentKey}.`, '');
    if (!isObject(parent)) {
      if (Array.isArray(parent)) {
        const values = parent.map(p => valueForKey(p, childKey));
        return clean(values, true).join(',');
      }
      return;
    }
    return valueForKey(parent, childKey);
  }
  value = object[key];

  return value;
};

const deleteKey = (obj, keyPath) => {
  const mutableObj = {
    ...obj,
  };

  const keyArray = keyPath.split('.');

  let mainObj = mutableObj;

  for (let i = 0; i < keyArray.length - 1; i++) {
    const key = keyArray[i];
    if (!isObject(mainObj)) {
      return mutableObj;
    }
    mainObj = mainObj[key];
  }

  const keyToRemove = keyArray.pop();
  delete mainObj[keyToRemove];
  return mutableObj;
};

/**
 * Returns the real typeof for an entity.
 * typeof returns 'object' for array and null, which seems
 * wrong. This function returns 'array' for array & 'null'
 * for null.
 * @param  {[String|Number|...]} entity   Entity to get type of
 * @return {String}                       The type of the entity
 */
export const realTypeOf = (entity) => {
  let type = typeof entity;
  if (type === 'object' && Array.isArray(entity)) {
    type = 'array';
  } else if (entity instanceof Set) {
    type = 'set';
  }
  if (entity === null) {
    type = 'null';
  }
  return type;
};

/**
 * Returns a new object with fieldsToRemove removed from
 * the object
 * @param  {Object} object              The object to remove fields
 *                                      from
 * @param  {Array} fieldsToRemove       An array of fields to remove
 * @return {Object}                     New object with fields removed
 */
export const removeFields = (object, fieldsToRemove) => {
  if (!isObject(object)) {
    throw new Error('Cannot remove fields because object provided is not an object');
  }
  if (!Array.isArray(fieldsToRemove)) {
    throw new Error('Expected fields to remove to be an array of strings');
  }

  let mutableObject = {
    ...object,
  };

  if (fieldsToRemove.length > 0) {
    fieldsToRemove.forEach((fieldToRemove) => {
      if (typeof valueForKey(mutableObject, fieldToRemove) !== 'undefined') {
        mutableObject = deleteKey(mutableObject, fieldToRemove);
      }
    });
  }

  return mutableObject;
};

/**
 * Returns whether an entity of any type is empty.
 * For objects, an object is empty if it has no keys
 * An array or a set is empty if it has not items in it
 * A string is empty if it's = ''. A string is not empty if it's filled
 * with whitespace
 * A null && undefined are always empty
 * A number,bool,Promise is never empty
 * @param {any} entity            Entity to check for emptyness
 * @returns {bool}                true if the entity is empty, false if it's not empty
 */
export const isEmpty = (entity) => {
  if (entity === undefined || entity === null) {
    return true;
  }
  const typeofEntity = realTypeOf(entity);

  switch (typeofEntity.toLowerCase()) {
    case 'string':
      return entity === '';
    case 'array':
      return entity.length === 0;
    case 'set':
      return entity.size === 0;
    case 'object':
      return Object.keys(entity).length === 0;
    default:
      return false;
  }
};

/**
 * Checks whether an object conforms to object shape
 * @param  {Object} object            Object to test
 * @param  {Object} shape             Expected shape
 * @param  {String} [mode=strict]     Possible values, strict, easy.
 *                                    If strict, it won't allow extra keys.
 *                                    If easy, it'll only check if compulsory
 *                                    keys exist.
 *
 * @return {bool}                  True if object matches shape, else false
 */
export const validateShape = (object, shape, mode = 'strict') => {
  if (!isObject(object)) {
    return false;
  }

  const objectKeys = Object.keys(object);
  const keysAllowed = Object.keys(shape);

  const compulsaryKeys = keysAllowed.filter((key => !shape[key].optional));

  // Check if it has all compulsary keys
  for (let i = 0; i < compulsaryKeys.length; i++) {
    const compulsaryKey = compulsaryKeys[i];
    if (!objectKeys.includes(compulsaryKey)) {
      return false;
    }
  }
  // check if it has keys that should'nt be there
  for (let i = 0; i < objectKeys.length; i++) {
    const objectKey = objectKeys[i];
    if (mode === 'strict') {
      if (!keysAllowed.includes(objectKey)) {
        return false;
      }
    }
    const data = object[objectKey];
    const objShape = shape[objectKey];
    if (objShape) {
      const {
        type,
      } = objShape;
      if (type !== undefined) {
        if (realTypeOf(data).toLowerCase() !== type.toLowerCase()) {
          return false;
        }
      }
    }
  }

  return true;
};


/**
 * Pads a string if length of string is less than required.
 * @param  {String} toPad          The string you want to pad
 * @param  {Number} requiredLength The required length of padded string
 * @param  {String} padWith        The string to pad with
 * @param  {String} padDirection   Can either be 'start' or 'end'. If start padding
 *                                 will be added in the starting of the string, if end
 *                                 then end
 * @return {String}                The padded string
 */
export const padString = (toPad, requiredLength, padWith, padDirection) => {
  const requiredPadding =
    requiredLength > toPad.length ?
      requiredLength - toPad.length :
      0;
  let paddedString = toPad;
  if (requiredPadding > 0) {
    let paddingAdded = 1;
    let padding = padWith;

    while (paddingAdded !== requiredPadding) {
      padding = `${padding}${padWith}`;
      paddingAdded += 1;
    }
    if (padDirection === 'start') {
      paddedString = `${padding}${toPad}`;
    } else {
      paddedString = `${toPad}${padding}`;
    }
  }
  return paddedString;
};





const cleanObject = (obj, removeEmptyString) => {
  const keys = Object.keys(obj);
  const cleanedObject = {};
  keys.forEach((key) => {
    if (isObject(obj[key])) {
      cleanedObject[key] = cleanObject(obj[key], removeEmptyString);
    } else if (obj[key] !== undefined && obj[key] !== null) {
      if (removeEmptyString) {
        if (obj[key] !== '') {
          cleanedObject[key] = obj[key];
        }
      } else {
        cleanedObject[key] = obj[key];
      }
    }
  });
  return cleanedObject;
};

/**
 * Takes an array and removes items whose value is falsy.
 * Works for array of objects and string, numbers etc
 * @param  {Array} toClean An array to clean
 * @return {Array}         A cleaned version of the input
 */
export const clean = (toClean, removeEmptyString = false, uniformCase = false, keyCase = 'lower') => {
  if (typeof toClean !== 'object') {
    throw new Error(`Clean can only clean Arrays or Objects. You sent ${typeof toClean} to clean`);
  }
  const cleaned = [];
  if (Array.isArray(toClean)) {
    toClean.forEach((item) => {
      if (item && isObject(item)) {
        const cleanedObj = cleanObject(item);
        if (Object.keys(cleanedObj).length > 0) {
          cleaned.push(cleanObject(item, removeEmptyString));
        }
      } else if (item) {
        if (removeEmptyString) {
          if (item !== '') {
            cleaned.push(item);
          }
        } else {
          cleaned.push(item);
        }
      }
    });
  } else if (isObject(toClean)) {
    const cleanedObject = cleanObject(toClean, removeEmptyString);
    if (uniformCase) {
      const dbObj = {};
      Object.keys(cleanedObject).forEach((key) => {
        let keyFormat = key;
        if (keyCase === 'lower') {
          keyFormat = key.toLowerCase();
        } else if (keyCase === 'upper') {
          keyFormat = key.toUpperCase();
        }
        dbObj[keyFormat] = cleanedObject[key];
      });
      return dbObj;
    }
  }
  return cleaned;
};

/**
 * Checks if all elements from subArray exists in superArray
 * If all elements from subArray exist in superArray, it'll
 * return true otherwise false. Only works for arrays of primary
 * items. Does not work for arrays with object
 * @param  {Array} superArray The superset array.
 * @param  {Array} subArray   The subset array.
 * @return {bool}
 */
export const arrayContains = (superArray, subArray) => {
  if (!superArray || !subArray) {
    console.log('Either is undefined');
    throw new Error('Cannot check arrayContains for undefined');
  }
  if (!Array.isArray(superArray) || !Array.isArray(subArray)) {
    console.log('Either is not an array');
    throw new Error('Cannot check contains because one of the params is not an array');
  }

  const superArrayLength = superArray.length;
  const subArrayLength = subArray.length;

  let contains = true;
  if (superArrayLength > 0 && subArrayLength > 0) {
    for (let i = subArrayLength - 1; i >= 0; i--) {
      const item = subArray[i];
      if (!superArray.includes(item)) {
        contains = false;
        break;
      }
    }
  } else {
    contains = false;
  }

  return contains;
};

/**
 * Returns a simplified object after parsing info in rdData.
 * @param  {Object} rdData         RDData object
 * @param  {Array}  rdData.info    An array of objects containing key and value
 * @return {Object}                A simplified easier to work with version
 *                                 of rdData
 */
export const simplifyRDData = (rdData) => {
  // const infoKeyKey = 'field_key';
  const infoValueKey = 'field_value';

  if (!rdData) {
    throw new Error('Cannot simplify rdData because rdData is not valid');
  }

  if (typeof rdData !== 'object' || Array.isArray(rdData)) {
    throw new Error('rdData is not an object');
  }

  if (rdData.info == null) {
    try {
      console.log(JSON.stringify(rdData, null, 2));
    } catch (e) {}
    throw new Error('rdData.info should be an object but is undefined or null');
  }

  if (!isObject(rdData.info)) {
    throw new Error('rdData.info is not an object');
  }

  const info = { ...rdData.info };
  const simpleInfo = {};
  const infoKeys = Object.keys(info);
  if (infoKeys.length > 0) {
    infoKeys.forEach((infoKey) => {
      const infoItem = info[infoKey];
      if (isObject(infoItem)) {
        const infoItemValue = infoItem[infoValueKey];
        simpleInfo[infoKey] = infoItemValue;
      }
    });
  }

  const simplifiedRdData = {
    ...rdData,
    ...{
      info: simpleInfo,
    },
  };
  return simplifiedRdData;
};

export const complexifyRDData = (rdData) => {

  if (!rdData) {
    throw new Error('Cannot complexify rdData because rdData is not valid');
  }

  if (typeof rdData !== 'object' || Array.isArray(rdData)) {
    throw new Error('rdData is not an object');
  }

  if (rdData.info == null) {
    throw new Error('rdData.info should be an object but is undefined or null');
  }

  if (!isObject(rdData.info)) {
    throw new Error('rdData.info is not an object');
  }

  const info = { ...rdData.info };
  const complexInfo = {};
  const rdDataKeys = Object.keys(info);
  if (rdDataKeys.length > 0) {
    rdDataKeys.forEach((rdDataKey) => {
      complexInfo[rdDataKey] = {
        field_type: 'String',
        field_value: info[rdDataKey],
      };
    });
  }

  const complexifiedRdData = {
    ...{
      info: complexInfo,
    },
  };
  return complexifiedRdData;
};

/**
 * Replaces all placeholders (strings surrounded with {{}})
 * @param  {String} template              String with placeholders to replace
 * @param  {String} [replaceWith = '']    String to replace placeholders with
 * @return {String}                       String with all data replaced
 */
export const replacePlaceholders = (template, replaceWith = '') => {
  if (template === undefined || template === null) {
    return '';
  }
  let cleanReplaceWith = replaceWith;
  if (typeof cleanReplaceWith !== 'string') {
    cleanReplaceWith = '';
  }
  const placeholderCaptureRegex = /{{[.\w]+}}/g;
  return template.replace(placeholderCaptureRegex, cleanReplaceWith);
};

/*
 * Takes a template string and fills variables with values from content
 * The variables in the template should be surrounded by double curly
 * braces, example {{variableName}}. If variable value is expected to
 * be of type Date, you can add a dateformat to the variable, Example:
 * {{create_date|DD/MM/YYYY}}
 * @param  {String} template The template string
 * @param  {Object} content  Object with value for variables in template
 *                           string
 * @param  {Bool|String}  [replaceUnfoundDataWithVariableName=true]   If true and
 *                           value for key not found in content, it'll replace
 *                           the placeholder with variable name. Eg:
 *                           "I am a {{occupation}}" will become "I am a occupation"
 *                           If set to false, it'll remove the placeholder. Eg:
 *                           "I am a {{occupation}}" will become "I am a ".
 *                           Defaults to true. If type is string and value is 'strict',
 *                           it will return unfound variables untouched.
 * @param  {Object} mapping? Optional object. If exists, hydrate string will try to
 *                           map the data value from this mapping object. Eg:
 *                           For template "I am a {{occupation}}" with mapping
 *                           {
 *                             occupation: {
 *                               1: "Salesman",
 *                               2: "Plumber",
 *                               3: "Developer",
 *                               default: "Human"
 *                             }
 *                           } and content { occupation: 2 }, the return will be
 *                           "I am a Plumber". You can set a default to mapping so
 *                           unfound data will be mapped to default. If no default found,
 *                           it'll not replace the data
 * @param  {bool} jsonStringifyObjects   If true, values that are objects or arrays, will be
 *                                       JSON stringified. Defaults to false
 * @return {String}          Template string hydrated with value
 */
export const hydrateString = (
  template,
  content,
  replaceUnfoundDataWithVariableName = true,
  mapping,
  jsonStringifyObjects = false,
) => {
  if (!template) {
    throw new Error('Template string is undefined');
  }
  if (typeof template !== 'string') {
    throw new Error('Template string should be a string');
  }

  if (!content) {
    throw new Error('Content cannot be undefined');
  }
  if (!isObject(content)) {
    throw new Error('Content should be an object');
  }

  if (typeof mapping !== 'undefined') {
    if (!isObject(content)) {
      throw new Error('Content Mapping should be an object');
    }
  }

  const placeholderCaptureRegex = /{{[.\w]+}}/g;
  const specialPlaceholderCaptureRegex = /{{[.\w]+[|]{1}\w+[^\}}]+}}/g;
  const placeholders = template.match(placeholderCaptureRegex);
  const specialPlaceholders = template.match(specialPlaceholderCaptureRegex);

  let mutableTemplate = template;

  if (placeholders) {
    placeholders.forEach((placeholder) => {
      const valueAddress = placeholder
        .replace('{{', '')
        .replace('}}', '');
      if (valueAddress.includes('.')) {
        let value = valueForKey(content, valueAddress);
        if (typeof value === 'undefined') {
          if (typeof replaceUnfoundDataWithVariableName === 'boolean') {
            value = replaceUnfoundDataWithVariableName ? valueAddress : '';
          } else if (replaceUnfoundDataWithVariableName === 'strict') {
            value = placeholder;
          }
        }

        let mappedValue = value;
        if (typeof mapping !== 'undefined') {
          if (typeof mapping[placeholder] !== 'undefined') {
            mappedValue = mapping[valueAddress][value];
            if (typeof mappedValue === 'undefined') {
              mappedValue = mapping[valueAddress].default || '';
            }
          }
        }
        mutableTemplate = mutableTemplate.replace(new RegExp(placeholder, 'g'), mappedValue);
      } else {
        let value = valueForKey(content, valueAddress);
        if (typeof value === 'undefined') {
          if (typeof replaceUnfoundDataWithVariableName === 'boolean') {
            value = replaceUnfoundDataWithVariableName ? valueAddress : '';
          } else if (replaceUnfoundDataWithVariableName === 'strict') {
            value = placeholder;
          }
        } else if (isObject(value) || Array.isArray(value)) {
          if (jsonStringifyObjects) {
            value = JSON.stringify(value);
          }
        }

        let mappedValue = value;
        if (typeof mapping !== 'undefined') {
          if (typeof mapping[valueAddress] !== 'undefined') {
            mappedValue = mapping[valueAddress][value];
            if (typeof mappedValue === 'undefined') {
              mappedValue = mapping[valueAddress].default || '';
            }
          }
        }
        mutableTemplate = mutableTemplate.replace(new RegExp(placeholder, 'g'), mappedValue);
      }
    });
  }

  if (specialPlaceholders && specialPlaceholders.length > 0) {
    /* eslint-disable */
    specialPlaceholders.forEach((specialVariable) => {
      const variable = specialVariable
        .replace('{{', '')
        .replace('}}', '');
      const variableName = variable.split('|')[0];
      const variableFormat = variable.split('|')[1];

      let mutableContent = { ...content };
      let mutableVariableName = variableName;
      if (mutableVariableName) {
        // Support for subobject
        while(mutableVariableName.indexOf('.') > -1) {
          var objectKey = mutableVariableName.split('.')[0];
          mutableContent = mutableContent[objectKey];
          if (mutableContent === undefined) {
            console.error(`no content found for key ${variableName}`);
            break;
          }
          mutableVariableName = mutableVariableName.split('.')[1];
        }
        // Check if date format
        const dateString = mutableContent[mutableVariableName];
        if (typeof dateString !== 'undefined') {
          const momentDate = moment(dateString);
          if (momentDate.isValid()) {
            const formattedDateString = moment(momentDate).format(variableFormat);
            mutableTemplate = mutableTemplate
              .replace(specialVariable, formattedDateString);
          } else {
            console.error(`${dateString} is not a valid date for variableName: ${variableName}`);
            console.log(`Will replace ${specialVariable} without trying to format to ${content[key]}`);
            mutableTemplate = mutableTemplate.replace(specialVariable, content[key]);
          }
        } else {
          let value = '';
          if (typeof replaceUnfoundDataWithVariableName === 'boolean') {
            value = replaceUnfoundDataWithVariableName ? variableName : '';
          } else if (replaceUnfoundDataWithVariableName === 'strict') {
            value = specialVariable;
          }
          mutableTemplate = mutableTemplate.replace(specialVariable, value);
        }
      }
    });
    /* eslint-enable */
  }

  return mutableTemplate;
};


export const getParametersFromString = (parametersString) => {
  // Remove the surrounding brackets and split into an array
  // and trim extra whitespace
  const reverseCharForObjectStartChar = (char) => {
    if (char === '{') {
      return '}';
    } else if (char === '[') {
      return ']';
    } else if (char === '{[') {
      return ']}';
    } else if (char === '[{') {
      return '}]';
    }
  };
  const parameterArray = [];
  let parameterObjectStartChar = '';
  let parameterObjectEndChar = '';
  let parameterObjectStartCharCount = 0;
  let parameterObjectBuffer = '';
  let parameterObjectBufferModeOn = false;

  if (parametersString.includes('{') || parametersString.includes('[')) {
    // JSON found
    parametersString
      .substring(1, parametersString.length - 1)
      .split(',')
      .forEach((parameter) => {
        let cleanedParameter = parameter.trim();
        if (cleanedParameter === 'true') {
          cleanedParameter = true;
        } else if (cleanedParameter === 'false') {
          cleanedParameter = false;
        }
        if (parameterObjectBufferModeOn) {
          if (cleanedParameter.endsWith(parameterObjectEndChar)
            && parameterObjectStartCharCount === 1) {
            parameterObjectBuffer = `${parameterObjectBuffer}${cleanedParameter}`;
            parameterArray.push(parameterObjectBuffer);
            parameterObjectBufferModeOn = false;
            parameterObjectStartChar = '';
            parameterObjectEndChar = '';
            parameterObjectStartCharCount = 0;
            parameterObjectBuffer = '';
          } else {
            if (cleanedParameter.includes(parameterObjectStartChar)) {
              const match = cleanedParameter.match(new RegExp(`\\${parameterObjectStartChar}`, 'g'));
              const numberOfOccurrences = match.length;
              parameterObjectStartCharCount += numberOfOccurrences;
            }
            if (cleanedParameter.includes(parameterObjectEndChar)) {
              const match = cleanedParameter.match(new RegExp(`\\${parameterObjectEndChar}`, 'g'));
              const numberOfOccurrences = match.length;
              parameterObjectStartCharCount -= numberOfOccurrences;
            }
            parameterObjectBuffer = `${parameterObjectBuffer}${cleanedParameter},`;
          }
        } else if (
          typeof cleanedParameter === 'string' &&
          (cleanedParameter.startsWith('[') ||
          cleanedParameter.startsWith('{')) &&
          !parameterObjectBufferModeOn &&
          !cleanedParameter.startsWith('{{')) {
          parameterObjectStartChar = cleanedParameter.charAt(0);
          parameterObjectEndChar = reverseCharForObjectStartChar(parameterObjectStartChar);
          parameterObjectStartCharCount = 1;
          parameterObjectBuffer = `${parameterObjectBuffer}${cleanedParameter},`;
          parameterObjectBufferModeOn = true;
        } else {
          parameterArray.push(cleanedParameter);
        }
      });
  } else {
    parametersString
      .substring(1, parametersString.length - 1)
      .split(',')
      .forEach((parameter) => {
        let cleanedParameter = parameter.trim();
        if (cleanedParameter === 'true') {
          cleanedParameter = true;
        } else if (cleanedParameter === 'false') {
          cleanedParameter = false;
        }
        parameterArray.push(cleanedParameter);
      });
  }
  return parameterArray;
};

/**
 * Evaluates template and returns value. For data placeholders, uses hydrateString
 * For function evaluators, will process template method call. For functions, template
 * should be of the format, $functionName(param1, param2). The function name should be
 * prefixed by a $ sign.
 * @param  {String} template                   The template string.
 *                                             Example: '{{context.user.name}} is
 *                                             $fromNow({{context.user.birthdate}}, years)
 *                                             years old'
 *                                             Function names should be supported by evalFunctions.
 *                                             If the function throws an error or function does not
 *                                             exist, function will be replaced with an empty string
 * @param  {Object} context                    Object with data to be used to replace placeholders
 * @param  {Bool| String} [replaceUnfoundDataWithVariableName = true]
 *                                             Replaces not found data with variable name.
 *                                             Defaults to true
 * @param  {Object} [mapping]                  Object to map value. Sent to hydrateString
 * @return {String}                            Template string with placeholders replaced.
 */
export const evaluate = (template, context, replaceUnfoundDataWithVariableName = true, mapping) => {
  const hydratedTemplate = hydrateString(
    template,
    context,
    replaceUnfoundDataWithVariableName,
    mapping,
    true,
  );
  let evaluatedTemplate = hydratedTemplate;

  const matchMethodCaptureRegex = /\$\w+[(]{2}[\w\d\s,:.@&~!?#%*/${}<>"'[\]/();=\u0E01-\u0E5B\\_-]+[)]{2}/g;
  const functionNameCaptureRegex = /\$\w+\(/g;
  // const functionParametersCaptureRegex = /\((.*?)\)/g;

  const functionCalls = evaluatedTemplate.match(matchMethodCaptureRegex);
  // Check if function evaluation is needed
  if (functionCalls && functionCalls.length > 0) {
    // Loop through all functions in template
    functionCalls.forEach((functionCall) => {
      let functionName = functionCall.match(functionNameCaptureRegex)[0];
      // const oldparameters = functionCall.match(functionParametersCaptureRegex)[0];
      let functionCallReturn = '';
      // Remove the $ and first bracket
      functionName = functionName.substring(1, functionName.length - 1);
      let parameters = functionCall
        .replace(functionName, '');
      parameters = parameters.substring(2, parameters.length - 1);

      if (functionName.trim().length > 0) {
        let parameterArray = [];
        if (parameters && parameters.trim().length > 0) {
          parameterArray = getParametersFromString(parameters);
        }
        const func = evalFunctions[functionName];
        if (typeof func === 'function') {
          try {
            functionCallReturn = func(...parameterArray);
          } catch (e) {
            console.error(`Calling function ${functionCall} threw an error, ${e}`);
          }
        } else {
          console.error(`Function ${functionName} is not supported.
            Supported functions are ${Object.keys(evalFunctions).join(',')}`);
        }

        // Replace function calls in evaluatedTemplate with return from functions
        // evaluatedTemplate = evaluatedTemplate.replace(
        //   new RegExp(cleanedFunctionCall, 'g'),
        //   functionCallReturn,
        // );
        evaluatedTemplate = evaluatedTemplate
          .split(functionCall)
          .join(functionCallReturn);
      }
    });
  }
  // Return evaluatedTemplate
  return evaluatedTemplate;
};

/**
 * Hydrates an object template. Changes placeholder data with data from context object
 * and returns a new object with placeholder values replaced.
 * @param  {Object} objectTemplate            Object to hydrate
 * @param  {Object} context                   Object to use to hydrate template
 * @param  {String} [contextKey=context]      Key name used in template. Defaults to
 *                                            'context'
 * @return {Object}                           Hydrated object
 */
export const hydrateObject = (objectTemplate, context, contextKey = 'context') => {
  if (!isObject(objectTemplate)) {
    console.error('Object template should be an object');
    throw new Error('Object template should be an object');
  }
  if (!isObject(context)) {
    console.error('Context should be an object');
    throw new Error('Context should be an object');
  }
  if (typeof contextKey !== 'string') {
    console.error('Placeholder key should be a string');
    throw new Error('Placeholder key should be a string');
  }

  const hydratedObject = { ...objectTemplate };
  Object.keys(hydratedObject).forEach((key) => {
    const keyValue = hydratedObject[key];
    if (keyValue === `{{${contextKey}}}`) {
      hydratedObject[key] = context;
    } else {
      const singleValueCaptureRegex = /^\{{2}[.\w]+\}{2}$/g;
      const keyCaptureRegex = /{{[.\w]+}}/g;
      const specialKeyCaptureRegex = /{{[.\w]+[|]{1}[\w/+]+}}/g;
      const functionCaptueRegex = /[$]{1}\w+[(]{2}.+[)]{2}/g;

      if (isObject(keyValue)) {
        // Recursive
        hydratedObject[key] = hydrateObject(keyValue, context, contextKey);
      } else if (Array.isArray(keyValue)) {
        hydratedObject[key] = keyValue.map((keyValueItem) => {
          let mutableKeyValueItem = keyValueItem;
          if (typeof keyValueItem === 'string') {
            mutableKeyValueItem = keyValueItem.replace(/\bcontext.\b/g, '');
            return evaluate(mutableKeyValueItem, context, false);
          }
          return hydrateObject(mutableKeyValueItem, context);
        });
      } else if (singleValueCaptureRegex.test(keyValue)) {
        // Get actual key name
        let keyToReplace = keyValue.replace(/[{}]/g, '');
        let dataObject = { ...context };
        dataObject[contextKey] = context;
        while (isObject(dataObject) && keyToReplace.includes('.')) {
          const parentKey = keyToReplace.split('.')[0];
          dataObject = dataObject[parentKey] || {};
          [, keyToReplace] = keyToReplace.split(`${parentKey}.`);
        }
        hydratedObject[key] = dataObject[keyToReplace];
      } else if (keyCaptureRegex.test(keyValue)) {
        // Workaround. Strip "context." if exists
        const strippedKeyValue = keyValue.replace(/\bcontext.\b/g, '');
        const replaceUnfound = functionCaptueRegex.test(keyValue) ? 'strict' : false;
        hydratedObject[key] = evaluate(strippedKeyValue, context, replaceUnfound);
      } else if (specialKeyCaptureRegex.test(keyValue)) {
        let keyToReplace = keyValue.replace(/[{}]/g, '');
        let dataObject = {};
        dataObject[contextKey] = context;
        while (isObject(dataObject) && keyToReplace.includes('.')) {
          const parentKey = keyToReplace.split('.')[0];
          dataObject = dataObject[parentKey] || {};
          [, keyToReplace] = keyToReplace.split(`${parentKey}.`);
        }
        hydratedObject[key] = evaluate(`{{${keyToReplace}}}`, dataObject, false);
      } else if (functionCaptueRegex.test(keyValue)) {
        hydratedObject[key] = evaluate(keyValue, context, 'strict');
      }
    }
  });
  return hydratedObject;
};

/**
 * Returns a result of array.join() where array consists template
 * hydrated for each item in items array. Uses hydrateString.
 * @param  {String} template                            The template string
 * @param  {Array}  items                               An array of items where each item is
 *                                                      passed as context to hydrateString
 * @param  {Bool} [shouldJoin = false]                  If true, it'll join the array of hydrated
 *                                                      templates and will return an array. Defaults
 *                                                      to false.
 * @param  {String} [joinBy = '']                       String used to join hydrated templates.
 *                                                      Defaults to empty string
 * @param  {Bool} [wrapJoinByWithWhitespace = false]    If true, it will wrap joinBy string with
 *                                                      whitespace. Defaults to false.
 * @return {String}                                     All hydrated templates concatenated
 *                                                      together
 */
export const hydrateTemplate = (template, items = [], shouldJoin = false, joinBy = '', wrapJoinByWithWhitespace = false) => {
  if (typeof template !== 'string') {
    throw new Error(`Expected template to be a string, instead got ${realTypeOf(template)}`);
  }
  if (!Array.isArray(items) && typeof items !== 'string') {
    throw new Error(`Expected items to be an array or a json string, instead got ${realTypeOf(items)}`);
  }

  let cleanedItems = items;
  if (Array.isArray(items)) {
    cleanedItems = [...items];
  } else {
    // Check if JSON stringifiable
    try {
      cleanedItems = JSON.parse(items);
    } catch (e) {
      console.log('Parsing cleanedItems failed');
      console.error('Items cannot be parsed as a JSON Object');
      console.error(JSON.stringify(cleanedItems, null, 2));
      console.error(e);
      throw new Error(`hydrateTemplate: Items ${items} cannot be parsed as a JSON object`);
    }
    if (!Array.isArray(cleanedItems)) {
      console.error('Items is not an array');
      throw new Error(`hydrateTemplate: Expected parse JSON item to be an array, instead got ${realTypeOf(cleanedItems)}`);
    }
  }
  let cleanedJoinBy = joinBy;
  if (typeof cleanedJoinBy !== 'string') {
    console.warn(`hydrateTemplate: Cannot join by ${joinBy} because it is not a string.
      Defaulting to empty string`);
    cleanedJoinBy = '';
  }
  if (wrapJoinByWithWhitespace) {
    cleanedJoinBy = ` ${cleanedJoinBy} `;
  }

  if (cleanedItems.length > 0) {
    const hydratedTemplates = cleanedItems.map((item) => {
      const hydratedString = evaluate(template, item, false);
      return hydratedString;
    });
    if (shouldJoin) {
      return hydratedTemplates.join(cleanedJoinBy);
    }
    return hydratedTemplates;
  }
  return '';
};

export const mapDefinitionValueToContext = (context, definitions) => {
  if (context === undefined) {
    console.error('Context sent to mapDefinitionValueToContext is undefined');
    throw new Error('Context should not be undefined');
  }
  if (!isObject(context)) {
    console.error('Context sent to mapDefinitionValueToContext is not an object');
    throw new Error('Context should be an object');
  }
  if (definitions === undefined) {
    return context;
  }
  if (!Array.isArray(definitions)) {
    console.error('Definitions sent to mapDefinitionValueToContext is not an array');
    throw new Error('Definitions should be an array');
  }
  if (definitions.length === 0) {
    return context;
  }
  const mutableContext = { ...context };

  const keys = Object.keys(mutableContext);
  keys.forEach((key) => {
    const definition = definitions.find(def => def.variable === key);
    if (definition !== undefined &&
      definition.mapping !== undefined &&
      definition.mapping.length > 0) {
      const { mapping } = definition;
      const valueToMap = mutableContext[key];
      if (valueToMap !== undefined &&
        valueToMap !== null &&
        typeof valueToMap !== 'object') {
        const mappedObject = mapping.find(map => map.name === valueToMap);
        if (mappedObject !== undefined) {
          mutableContext[key] = mappedObject.description;
        }
      }
    }
  });

  return mutableContext;
};

const reverseObject = (object) => {
  const reversedObject = {};
  if (isObject(object)) {
    const objectKeys = Object.keys(object);
    objectKeys.forEach((objectKey) => {
      const value = object[objectKey];
      if (value !== undefined && value !== null && typeof value !== 'object') {
        reversedObject[value] = objectKey;
      }
    });
  }
  return reversedObject;
};

/**
 * Maps values in object with values from another object where
 * value from object 1 matches key from object 2. Example:
 *
 * object = {
 *   foo: 'bar'
 * };
 * context = {
 *   bar: 'tomorrow 10:30 p.m'
 * };
 *
 * mapValues(object, context) = {
 *   foo: 'tomorrow 10:30 p.m'
 * }
 * @param  {Object|Array} object  Object where value is replaced
 * @param  {Object} context       Object whose values are used to replace
 *                                value in object
 * @return {Object}               Object with replaced values
 */
export const mapValues = (object, context) => {
  if (!isObject(object) && !Array.isArray(object)) {
    throw new Error(`Expected object to be an object or an array, instead received ${realTypeOf(object)}`);
  }
  if (!isObject(context)) {
    throw new Error(`Expected context to be an object, instead received ${realTypeOf(context)}`);
  }
  const contextKeys = Object.keys(context);
  if (isObject(object)) {
    const mutableObject = { ...object };
    const mutableObjectKeys = Object.keys(mutableObject);

    if (mutableObjectKeys.length > 0 && contextKeys.length > 0) {
      const reverseMutableObject = reverseObject(mutableObject);
      const reverseMutableObjectKeys = Object.keys(reverseMutableObject);

      contextKeys.forEach((contextKey) => {
        if (reverseMutableObjectKeys.includes(contextKey)) {
          const mutableObjectKey = reverseMutableObject[contextKey];
          mutableObject[mutableObjectKey] = context[contextKey];
        }
      });
    }
    return mutableObject;
  } else if (Array.isArray(object)) {
    const mutableArray = object.map((objectItem) => {
      if (contextKeys.includes(objectItem)) {
        return context[objectItem];
      }
      return objectItem;
    });
    return mutableArray;
  }
};

/**
 * Utility function to add delay mid execution of your code.
 * sleep returns a promise and uses setTimeout to delay. To use sleep,
 * your code will need to `await` it. Eg:
 *
 * import { sleep } from 'Alfred'
 *
 * console.log('Going to sleep')
 * await sleep(300)
 * console.log('Awake Now')
 *
 * On running the above code, there should be a ~300 secs delay between
 * the first log statement and the last log statement.
 *
 * Note: Because we are using setTimeout, it does not guarantee that the sleep
 * amount will be exactly as the amount it should be. This is because of the
 * way node works. If you want to know more, read about Event Loop and Call Stack
 * in nodejs
 *
 * @param  {Number} [waitInSeconds=50]        The seconds to sleep. Default to
 *                                            50 seconds
 * @return {}
 */
export const sleep = waitInSeconds => new Promise((resolve) => {
  let mutableWaitInSeconds = waitInSeconds || 50;
  if (Number.isNaN(mutableWaitInSeconds)) {
    // Defaults to 50 seconds
    mutableWaitInSeconds = 50;
  }
  const waitInMilliSeconds = 1000 * mutableWaitInSeconds;
  setTimeout(resolve, waitInMilliSeconds);
});

// Start of condition to object methods
const operatorKeyForOperator = (operator) => {
  const operators = {
    '=': '$eq',
    '>': '$gt',
    '<': '$lt',
    '>=': '$gte',
    '<=': '$lte',
  };

  return operators[operator];
};

const connectorKeyForConnector = (connector) => {
  const connectors = {
    and: '$and',
    or: '$or',
  };

  return connectors[connector];
};

export const parseCondition = (condition) => {
  const operators = ['=', '>', '<', '>=', '<='];
  let operator = '';
  let operatorKey = '';
  for (let i = 0; i < operators.length; i++) {
    const activeOperator = operators[i];
    if (condition.includes(activeOperator)) {
      operator = activeOperator;
      operatorKey = operatorKeyForOperator(operator);
      break;
    }
  }

  const conditionElements = condition.split(operator);
  const field = conditionElements[0]
    .trim()
    .replace(/\[/g, '')
    .replace(/\]/g, '');
  const value = conditionElements[1].trim();

  const conditionElementObject = {};
  conditionElementObject[operatorKey] = value;

  const conditionObject = {};
  conditionObject[field] = conditionElementObject;

  return conditionObject;
};

export const convertToConditionObject = (conditionString) => {
  const connectors = ['and', 'or'];

  const mutableConditionString = conditionString
    .replace(/AND/g, 'and')
    .replace(/OR/g, 'or');

  let numberOfConnectors = 0;
  connectors.forEach((connector) => {
    numberOfConnectors += countSubstringInString(mutableConditionString, ` ${connector} `);
  });

  let logicObject = {};
  if (numberOfConnectors === 0) {
    logicObject = parseCondition(mutableConditionString);
  } else if (numberOfConnectors === 1) {
    let connector = '';
    for (let i = 0; i < connectors.length; i++) {
      const activeConnector = connectors[i];
      if (mutableConditionString.includes(activeConnector)) {
        connector = activeConnector;
        break;
      }
    }
    const conditions = mutableConditionString.split(` ${connector} `);
    logicObject[connectorKeyForConnector(connector)] =
      conditions.map(condition => parseCondition(condition));
  } else {
    // TODO
  }

  return logicObject;
};
// End of condition to object methods

// Start of jexl condition methods
export const jexlify = (condition) => {
  const mutableCondition = condition
    .replace(/\[/g, '')
    .replace(/\]/g, '')
    .replace(/(\band\b|\bAND\b)(?=(?:[^"]*"[^"]*")*[^"]*$)\b(?=(?:[^']*'[^']*')*[^']*$)/g, '&&')
    .replace(/(\bor\b|\bOR\b)(?=(?:[^"]*"[^"]*")*[^"]*$)\b(?=(?:[^']*'[^']*')*[^']*$)/g, '||')
    .replace(/=/g, '==')
    .replace(/!==/g, '!=')
    .replace(/>==/g, '>=')
    .replace(/<==/g, '<=');

  return mutableCondition;
};

export const evaluateCondition = async (conditionString, context) => {
  if (conditionString) {
    jexl.addTransform('exists', (val) => {
      if (val != undefined) {
        if (typeof val === 'object') {
          if (isEmpty(val)) {
            return false;
          }
        }
        return true;
      }
      return false;
    });
    const jexedCondition = jexlify(conditionString);
    try {
      const result = await jexl.eval(jexedCondition, context);
      return new Promise(resolve => resolve(result));
    } catch (e) {
      console.error('Got error evaluating condition', e);
      return new Promise(resolve => resolve(false));
    }
  }
  return new Promise(resolve => resolve(false));
};

// End of jexl condition methods


/**
 * Format input data by idvar descriptive (single value)
 * @param  {Object} info    input data
 * @param  {Object} idvar   field's description from deployment
 * @return {Object} result data.
 */

export const singleInfoFormatter = (info, idvar) => {
  if (info && idvar) {
    const { field_value: fieldValue } = info;
    const { format, locale, field_type: fieldType } = idvar;
    let value = fieldValue;
    if (fieldType.toLowerCase() === 'date') {
      if (format && format !== '') {
        if (locale === 'th-TH') {
          //  workaround implement for buddhist year, until moment support buddhist year
          let date = moment(value, format);
          if (!date.isValid()) {
            date = moment(value);
          }
          if (date.isValid()) {
            value = date.subtract(543, 'year').format('YYYY-MM-DD HH:mm:ss');
          }
        } else {
          value = moment(value, format).format('YYYY-MM-DD HH:mm:ss');
        }
      }
    }

    return { ...info, field_value: value };
  }
  return info;
};

/**
 * Format input data by idvar descriptive
 * @param  {Object} info    input data
 * @param  {Object} idvar   field's description from deployment
 * @return {Object} result data.
 */

export const infoFormatter = (info, idvar) => {
  if (info && idvar) {
    const result = {};
    const keys = Object.keys(info);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (idvar[key]) {
        result[key] = singleInfoFormatter(info[key], idvar[key]);
      } else {
        result[key] = { ...info[key] };
      }
    }
    return result;
  }
  return info;
};

/**
 * Get Date By Period Time
 * @param {String} periodDuration period duration example 1M (1 month), 7D (7 days)
 * @param {String} from date time as string format
 * @param {String} fromFormat format of date time
 * @param {ENUM | String} direction date time that would like to get before(-)|after(+) from time
 * @return {String} date time as string format base on moment.
 */
export const getDateByPeriodTime = (periodDuration = '', from = '', fromFormat = '', direction = DIRECTION_PERIOD_TIME.BEFORE) => {
  if (!periodDuration || typeof periodDuration !== 'string') throw new Error('periodDuration must be string');
  if (!from) {
    // eslint-disable-next-line no-param-reassign
    from = moment();
  }
  if (typeof from !== 'string' && !(from instanceof moment)) throw new Error('from must be string');
  if (from && typeof from === 'string') {
    if (!fromFormat || typeof fromFormat !== 'string') throw new Error('fromFormat must be string');
  }
  if (!direction || typeof direction !== 'string') throw new Error('direction must be string');

  let isMatchDirection;
  Object.keys(DIRECTION_PERIOD_TIME).forEach((key) => {
    if (direction === DIRECTION_PERIOD_TIME[key]) isMatchDirection = true;
  });
  if (!isMatchDirection) throw new Error('direction is not match');

  // eslint-disable-next-line prefer-const
  let [time = 1, format = 'M'] = periodDuration.split(/(\d+)/).filter(Boolean);
  switch (format.toLowerCase()) {
    case 'q': case 'm':
      format = format.toLocaleUpperCase();
      break;
    case 'y': case 'w': case 'd':
      break;
    default:
      format = 'M';
      break;
  }

  let operator;
  switch (direction.valueOf()) {
    case 'before':
      operator = '-';
      break;
    case 'after': default:
      operator = '+';
      break;
  }

  return moment(from, fromFormat).utc().endOf('date')
    .add(`${operator}${parseInt(time, 10)}`, format)
    .toISOString();
};

export const dynamoItemToObj = (dynamoItem) => {
  const item = {};
  const dynamoKeys = Object.keys(dynamoItem);
  dynamoKeys.forEach((dynamoKey) => {
    const dynamoFieldObj = dynamoItem[dynamoKey];
    if (dynamoFieldObj) {
      const dynamoFieldObjKeys = Object.keys(dynamoFieldObj);
      const dynamoFieldTypeKey = dynamoFieldObjKeys[0];
      switch (dynamoFieldTypeKey) {
        case 'S':
          item[dynamoKey] = dynamoFieldObj[dynamoFieldTypeKey];
          break;
        case 'BOOL':
          item[dynamoKey] = dynamoFieldObj[dynamoFieldTypeKey];
          break;
        case 'N':
          item[dynamoKey] = Number.parseInt(dynamoFieldObj[dynamoFieldTypeKey], 10);
          break;
        case 'M':
          item[dynamoKey] = dynamoItemToObj(dynamoFieldObj[dynamoFieldTypeKey]);
          break;
        case 'L': {
          const dynamoFieldArray = dynamoFieldObj[dynamoFieldTypeKey];
          item[dynamoKey] = dynamoFieldArray.map((dynamoField) => {
            const parsedObj = dynamoItemToObj({ v: dynamoField });
            return parsedObj.v;
          });
          break;
        }
        default:
          break;
      }
    }
  });
  return item;
};

export default {
  extractBaseUrl,
  extractPath,
  chop,
  clean,
  arrayContains,
  isObject,
  isEmpty,
  validateShape,
  simplifyRDData,
  complexifyRDData,
  replacePlaceholders,
  convertToConditionObject,
  evaluateCondition,
  jexlify,
  hydrateString,
  hydrateObject,
  valueForKey,
  removeFields,
  mapDefinitionValueToContext,
  evaluate,
  mapValues,
  replaceValueFromMapping,
  realTypeOf,
  infoFormatter,
  singleInfoFormatter,
  padString,
  getParametersFromString,
  hydrateTemplate,
  sleep,
  getDateByPeriodTime,
  DIRECTION_PERIOD_TIME,
  dynamoItemToObj,
};
