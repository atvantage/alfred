"use strict";

var _moment = _interopRequireDefault(require("moment"));

var _ = _interopRequireDefault(require("./"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('fromNow', () => {
  it('defaults unit to days', () => {
    const dateString = '1991-02-05';
    const now = (0, _moment.default)();
    const date = (0, _moment.default)(dateString, 'YYYY-MM-DD');
    expect(_.default.fromNow(dateString)).toEqual(now.diff(date, 'days'));
  });
  it('defaults unit to days if unit supplied is not supported', () => {
    const dateString = '1991-02-05';
    const now = (0, _moment.default)();
    const date = (0, _moment.default)(dateString, 'YYYY-MM-DD');
    expect(_.default.fromNow(dateString, 'dogyears')).toEqual(now.diff(date, 'days'));
  });
  it('returns diff in units supplied if unit is supported', () => {
    const dateString = '1991-02-05';
    const now = (0, _moment.default)();
    const date = (0, _moment.default)(dateString, 'YYYY-MM-DD');
    expect(_.default.fromNow(dateString, 'years')).toEqual(now.diff(date, 'years'));
  });
  it('throws error if dateString is not valid', () => {
    const dateString = 'my birthday';
    expect(() => _.default.fromNow(dateString)).toThrow('Date my birthday is not valid');
  });
});