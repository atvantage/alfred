"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.today = exports.fromNow = void 0;

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Returns diff between input date and time as of when
 * the function was called
 * @param  {String} dateString  Ideally in ISO 8601 format but should
 *                              be fine as long as moment supports it.
 * @param  {String} unit        The unit of the difference wanted. Supported
 *                              are years, months, weeks, days, hours, minutes,
 *                              and seconds. Defaults to days
 * @return {Number}             Returns difference in dates.
 */
const fromNow = (dateString, unit = 'days') => {
  const supportedUnits = ['years', 'months', 'weeks', 'days', 'hours', 'minutes', 'seconds'];
  let verifiedUnit = unit;

  if (!supportedUnits.includes(verifiedUnit)) {
    console.log(`unit ${unit} is not supported by evalFunctions.fromNow. Defaulting to days.
    Supported units are ${supportedUnits.join(',')}`);
    verifiedUnit = 'days';
  }

  const now = (0, _moment.default)();
  const date = (0, _moment.default)(dateString);

  if (date.isValid()) {
    return now.diff(date, verifiedUnit);
  }

  throw new Error(`Date ${dateString} is not valid`);
};
/**
 * Returns a string representing the time called based on params
 * @param  {String} [format='dd/MM/yyyy']       Date Format
 * @param  {String} [utcOffset='+07:00']        UTC offset
 * @return {String}                             Returns string
 */


exports.fromNow = fromNow;

const today = (format = 'DD/MM/yyyy', utcOffset = '+07:00') => {
  const now = (0, _moment.default)().utcOffset(utcOffset);
  const nowString = now.format(format);
  return nowString;
};

exports.today = today;
var _default = {
  fromNow,
  today
};
exports.default = _default;